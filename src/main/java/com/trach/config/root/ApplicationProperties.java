package com.trach.config.root;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import lombok.Getter;

@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationProperties {

    @Value("${db.url}")
    private @Getter String dbUrl;
    @Value("${db.driver}")
    private @Getter String dbDriver;
    @Value("${db.username}")
    private @Getter String dbUsername;
    @Value("${db.password}")
    private @Getter String dbPassword;

    @Value("${hibernate.hbm2ddl.auto}")
    private @Getter String hibernateHdm2DdlAuto;
    @Value("${hibernate.show_sql}")
    private @Getter String hibernateShowSql;
    @Value("${hibernate.format_sql}")
    private @Getter String hibernateFormatSql;
    @Value("${hibernate.use_sql_comments}")
    private @Getter String hibernateUseSqlComments;
    @Value("${hibernate.dialect}")
    private @Getter String hibernateDialect;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}

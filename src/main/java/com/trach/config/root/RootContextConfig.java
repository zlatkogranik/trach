package com.trach.config.root;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.persistence.EntityManagerFactory;

/**
 * The root context configuration of the application - the beans in this context
 * will be globally visible in all servlet contexts.
 */

@Configuration
@EnableJpaRepositories(basePackages = "com.trach.app.jpa")
@ComponentScan({
  "com.trach.app.jpa",
  "com.trach.app.services"
})
public class RootContextConfig {

  @Bean(name = "transactionManager")
  public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory,
    DriverManagerDataSource dataSource) {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(entityManagerFactory);
    transactionManager.setDataSource(dataSource);
    return transactionManager;
  }

  @Bean(name = "multipartResolver")
  public CommonsMultipartResolver multipartResolver() {
    return new CommonsMultipartResolver();
  }

}

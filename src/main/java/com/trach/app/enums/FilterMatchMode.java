package com.trach.app.enums;

/**
 * Created by ToZla on 29/11/2016.
 */
public enum FilterMatchMode {

  LIKE("like"),

  EQUAL("="),

  NOT_EQUAL("!="),

  GREATER(">"),

  LESS("<"),

  GREATER_OR_EQUAL(">="),

  LESS_OR_EQUAL("<="),

  IN("in");

  private String sign;

  FilterMatchMode(String sign) {
    this.sign = sign;
  }

}

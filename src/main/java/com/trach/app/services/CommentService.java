package com.trach.app.services;

import com.trach.app.dto.CommentDTO;
import com.trach.app.dto.PageDTO;
import com.trach.app.enums.EmotionType;
import com.trach.app.jpa.CommentRepository;
import com.trach.app.jpa.EmotionRepository;
import com.trach.app.jpa.TrachRepository;
import com.trach.app.mappers.CommentDTOMapper;
import com.trach.app.model.Comment;
import com.trach.app.model.Emotion;
import com.trach.app.model.Trach;
import com.trach.app.rest.context.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class CommentService {

  @Autowired
  private CommentRepository commentRepository;

  @Autowired
  private TrachRepository trachRepository;

  @Autowired
  private EmotionRepository emotionRepository;

  @Autowired
  private SecurityService securityService;

  @Transactional(readOnly = true)
  public List<CommentDTO> findCommentsByTrachId(Integer id) {

    List<Comment> comment = commentRepository.findCommentsByTrachId(id);

    return CommentDTOMapper.INSTANCE.mapComments(comment);

  }

  @Transactional
  public CommentDTO saveCommentByTrachId(Integer trachId, CommentDTO commentDTO) {

    Trach trachById = trachRepository.findTrachById(trachId);

    Comment comment = Comment.builder()
                             .author(commentDTO.getAuthor())
                             .text(commentDTO.getText())
                             .date(new Date())
                             .likes(0)
                             .unlikes(0)
                             .trach(trachById)
                             .ipAddress(ThreadContext.getIpAddress())
                             .uuid(ThreadContext.getUuid())
                             .build();

    commentRepository.save(comment);

    return CommentDTOMapper.INSTANCE.map(comment);

  }

  @Transactional
  public void like(CommentDTO commentDTO) {

    if (!securityService.validCommentEmotionRequest(commentDTO, EmotionType.LIKE)) {
      return;
    }

    Emotion trachEmotionUnlike = emotionRepository.findCommentEmotionByUUID(commentDTO.getId(), EmotionType.UNLIKE, ThreadContext.getUuid());
    Emotion trachEmotionLike = emotionRepository.findCommentEmotionByUUID(commentDTO.getId(), EmotionType.LIKE, ThreadContext.getUuid());

    if (trachEmotionLike == null) {

      Comment comment = commentRepository.findCommentById(commentDTO.getId());

      if (trachEmotionUnlike != null) {
        emotionRepository.deleteEmotionById(trachEmotionUnlike.getId());
        commentRepository.decreaseUnlike(commentDTO.getId());
        commentDTO.setUnlikes(commentDTO.getUnlikes() - 1);
      }

      Emotion emotion = Emotion.builder()
                               .trachId(comment.getTrach().getId())
                               .commentId(comment.getId())
                               .type(EmotionType.LIKE)
                               .ipAddress(ThreadContext.getIpAddress())
                               .uuid(ThreadContext.getUuid())
                               .build();

      emotionRepository.save(emotion);

      commentRepository.increaseLike(commentDTO.getId());
      commentDTO.setLikes(commentDTO.getLikes() + 1);
    }

  }

  @Transactional
  public void unlike(CommentDTO commentDTO) {

    if (!securityService.validCommentEmotionRequest(commentDTO, EmotionType.UNLIKE)) {
      return;
    }

    Emotion trachEmotionUnlike = emotionRepository.findCommentEmotionByUUID(commentDTO.getId(), EmotionType.UNLIKE, ThreadContext.getUuid());
    Emotion trachEmotionLike = emotionRepository.findCommentEmotionByUUID(commentDTO.getId(), EmotionType.LIKE, ThreadContext.getUuid());

    if (trachEmotionUnlike == null) {

      Comment comment = commentRepository.findCommentById(commentDTO.getId());

      if (trachEmotionLike != null) {
        emotionRepository.deleteEmotionById(trachEmotionLike.getId());
        commentRepository.decreaseLike(commentDTO.getId());
        commentDTO.setLikes(commentDTO.getLikes() - 1);
      }

      Emotion emotion = Emotion.builder()
                               .trachId(comment.getTrach().getId())
                               .commentId(comment.getId())
                               .type(EmotionType.UNLIKE)
                               .ipAddress(ThreadContext.getIpAddress())
                               .uuid(ThreadContext.getUuid())
                               .build();

      emotionRepository.save(emotion);

      commentRepository.increaseUnlike(commentDTO.getId());
      commentDTO.setUnlikes(commentDTO.getUnlikes() + 1);
    }

  }

  @Transactional
  public CommentDTO approve(CommentDTO commentDTO) {

    Comment commentById = commentRepository.findCommentById(commentDTO.getId());

    commentById.setText(commentDTO.getText());

    commentById.setApproved(true);

    trachRepository.increaseComment(commentById.getTrach().getId());

    commentRepository.update(commentById);

    return CommentDTOMapper.INSTANCE.map(commentById);

  }

  @Transactional
  public CommentDTO reject(CommentDTO commentDTO) {

    Comment commentById = commentRepository.findCommentById(commentDTO.getId());

    commentRepository.delete(commentById);

    return commentDTO;

  }

  @Transactional(readOnly = true)
  public Page<CommentDTO> findCommentDTOsPageable(PageDTO pageDTO) {

    Page<Comment> commentPage = findCommentsPageable(pageDTO);

    Pageable pageable = new PageRequest(pageDTO.getFirstResult() / pageDTO.getMaxResult(), pageDTO.getMaxResult());

    List<CommentDTO> commentDTOs = CommentDTOMapper.INSTANCE.mapComments((List<Comment>)commentPage.getContent());

    return new PageImpl<>(commentDTOs, pageable, commentPage.getTotalElements());
  }

  @Transactional(readOnly = true)
  public Page<Comment> findCommentsPageable(PageDTO pageDTO) {
    return commentRepository.findCommentsPageable(pageDTO);
  }

  @Transactional
  public CommentDTO update(CommentDTO commentDTO) {

    Comment commentById = commentRepository.findCommentById(commentDTO.getId());

    commentById.setText(commentDTO.getText());
    commentById.setAuthor(commentDTO.getAuthor());

    commentRepository.update(commentById);

    return CommentDTOMapper.INSTANCE.map(commentById);

  }

  public CommentDTO findCommentsById(Integer id) {

    Comment commentById = commentRepository.findCommentById(id);

    return CommentDTOMapper.INSTANCE.map(commentById);

  }
}

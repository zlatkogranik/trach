package com.trach.app.services;

import com.trach.app.dto.TagDTO;
import com.trach.app.jpa.TagRepository;
import com.trach.app.mappers.TagDTOMapper;
import com.trach.app.model.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class TagService {

  @Autowired
  private TagRepository tagRepository;

  public TagDTO findTagById(Integer id) {

    Tag trachById = tagRepository.findTagById(id);

    return TagDTOMapper.INSTANCE.map(trachById);

  }

  public TagDTO findTagsByText(String text) {

    Tag trachByText = tagRepository.findTagByText(text);

    return TagDTOMapper.INSTANCE.map(trachByText);

  }

  public Page<TagDTO> findTagsByPhrase(String phrase) {

    return null;

  }

}

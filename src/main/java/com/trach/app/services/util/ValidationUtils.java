package com.trach.app.services.util;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

public final class ValidationUtils {

  private ValidationUtils() {
    throw new NotImplementedException("Utility classes cannot be instantiated");
  }

  public static void assertNotBlank(String email, String message) {
    if (StringUtils.isBlank(email)) {
      throw new IllegalArgumentException(message);
    }
  }

  public static void assertMinimumLength(String email, int length, String message) {
    if (email.length() < length) {
      throw new IllegalArgumentException(message);
    }
  }

  public static void assertMatches(String email, Pattern regex, String message) {
    if (!regex.matcher(email).matches()) {
      throw new IllegalArgumentException(message);
    }
  }
}

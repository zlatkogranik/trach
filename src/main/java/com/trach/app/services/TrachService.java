package com.trach.app.services;

import com.trach.app.dto.PageDTO;
import com.trach.app.dto.TrachDTO;
import com.trach.app.dto.TrachDTOAdvance;
import com.trach.app.enums.EmotionType;
import com.trach.app.jpa.EmotionRepository;
import com.trach.app.jpa.TagRepository;
import com.trach.app.jpa.TrachRepository;
import com.trach.app.mappers.TrachDTOMapper;
import com.trach.app.model.Emotion;
import com.trach.app.model.Tag;
import com.trach.app.model.Trach;
import com.trach.app.rest.context.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TrachService {

  @Autowired
  private TrachRepository trachRepository;

  @Autowired
  private TagRepository tagRepository;

  @Autowired
  private EmotionRepository emotionRepository;

  @Autowired
  private SecurityService securityService;

  @Transactional(readOnly = true)
  public TrachDTOAdvance findTrachById(Integer id) {

    Trach trachById = trachRepository.findTrachById(id);

    return TrachDTOMapper.INSTANCE.mapAdvance(trachById);

  }

  @Transactional(readOnly = true)
  public Page<TrachDTOAdvance> findTrachDTOsPageable(PageDTO pageDTO) {

    Page<Trach> trachPage = findTrachesPageable(pageDTO);

    Pageable pageable = new PageRequest(pageDTO.getFirstResult() / pageDTO.getMaxResult(), pageDTO.getMaxResult());

    List<TrachDTOAdvance> trachDTOs = TrachDTOMapper.INSTANCE.mapAdvanceList((List<Trach>)trachPage.getContent());

    return new PageImpl<>(trachDTOs, pageable, trachPage.getTotalElements());
  }

  @Transactional(readOnly = true)
  public Page<Trach> findTrachesPageable(PageDTO pageDTO) {
    return trachRepository.findTrachesPageable(pageDTO);
  }

  @Transactional
  public TrachDTOAdvance saveTrach(TrachDTO trachDTO) {

    Trach trach = Trach.builder()
                       .id(trachDTO.getId())
                       .text(trachDTO.getText())
                       .date(new Date())
                       .likes(0)
                       .unlikes(0)
                       .shares(0)
                       .comment(0)
                       .ipAddress(ThreadContext.getIpAddress())
                       .uuid(ThreadContext.getUuid())
                       .build();

    trachRepository.saveOrUpdate(trach);

    return TrachDTOMapper.INSTANCE.mapAdvance(trach);

  }

  @Transactional
  public void like(TrachDTO trachDTO) {

    if (!securityService.validTrachEmotionRequest(trachDTO, EmotionType.LIKE)) {
      return;
    }

    Emotion trachEmotionUnlike = emotionRepository.findTrachEmotionByUUID(trachDTO.getId(), EmotionType.UNLIKE, ThreadContext.getUuid());
    Emotion trachEmotionLike = emotionRepository.findTrachEmotionByUUID(trachDTO.getId(), EmotionType.LIKE, ThreadContext.getUuid());

    if (trachEmotionLike == null) {

      if (trachEmotionUnlike != null) {
        emotionRepository.deleteEmotionById(trachEmotionUnlike.getId());
        trachRepository.decreaseUnlike(trachDTO.getId());
        trachDTO.setUnlikes(trachDTO.getUnlikes() - 1);
      }

      Emotion emotion = Emotion.builder()
                               .trachId(trachDTO.getId())
                               .type(EmotionType.LIKE)
                               .ipAddress(ThreadContext.getIpAddress())
                               .uuid(ThreadContext.getUuid())
                               .build();

      emotionRepository.save(emotion);

      trachRepository.increaseLike(trachDTO.getId());
      trachDTO.setLikes(trachDTO.getLikes() + 1);
    }

  }

  @Transactional
  public void unlike(TrachDTO trachDTO) {

    if (!securityService.validTrachEmotionRequest(trachDTO, EmotionType.UNLIKE)) {
      return;
    }

    Emotion trachEmotionUnlike = emotionRepository.findTrachEmotionByUUID(trachDTO.getId(), EmotionType.UNLIKE, ThreadContext.getUuid());
    Emotion trachEmotionLike = emotionRepository.findTrachEmotionByUUID(trachDTO.getId(), EmotionType.LIKE, ThreadContext.getUuid());

    if (trachEmotionUnlike == null) {

      if (trachEmotionLike != null) {
        emotionRepository.deleteEmotionById(trachEmotionLike.getId());
        trachRepository.decreaseLike(trachDTO.getId());
        trachDTO.setLikes(trachDTO.getLikes() - 1);
      }

      Emotion emotion = Emotion.builder()
                               .trachId(trachDTO.getId())
                               .type(EmotionType.UNLIKE)
                               .ipAddress(ThreadContext.getIpAddress())
                               .uuid(ThreadContext.getUuid())
                               .build();

      emotionRepository.save(emotion);

      trachRepository.increaseUnlike(trachDTO.getId());
      trachDTO.setUnlikes(trachDTO.getUnlikes() + 1);
    }

  }

  @Transactional
  public void share(TrachDTO trachDTO) {
    trachRepository.increaseShare(trachDTO.getId());
  }

  @Transactional
  public void comment(TrachDTO trachDTO) {
    trachRepository.increaseComment(trachDTO.getId());
  }

  @Transactional
  public TrachDTOAdvance approve(TrachDTO trachDTO) {

    Trach trachById = trachRepository.findTrachById(trachDTO.getId());

    trachById.setText(trachDTO.getText());

    parseHashTags(trachById);

    trachById.setApproved(true);

    trachRepository.update(trachById);

    return TrachDTOMapper.INSTANCE.mapAdvance(trachById);

  }

  private void parseHashTags(Trach trach) {

    Pattern pattern = Pattern.compile("#\\w+");
    Matcher matcher = pattern.matcher(trach.getText());

    trach.setTags(new ArrayList<>());

    while (matcher.find()) {

      String hashTag = matcher.group(0);

      Tag tag = tagRepository.findTagByText(hashTag);

      if (tag == null) {
        tag = Tag.builder().text(hashTag).count(1).build();
        tagRepository.save(tag);
      }
      else {
        tagRepository.increaseCount(hashTag);
      }

      trach.getTags().add(tag);

    }

    String trim = trach.getText().replaceAll("#\\w+", "").trim();

    trach.setText(trim);

  }

  @Transactional
  public TrachDTO reject(TrachDTO trachDTO) {

    Trach trachById = trachRepository.findTrachById(trachDTO.getId());

    trachRepository.delete(trachById);

    return trachDTO;

  }

}

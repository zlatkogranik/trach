package com.trach.app.services;

import com.trach.app.dto.CommentDTO;
import com.trach.app.dto.TrachDTO;
import com.trach.app.enums.EmotionType;
import com.trach.app.jpa.EmotionRepository;
import com.trach.app.model.Emotion;
import com.trach.app.rest.context.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ToZla on 14/03/2017.
 */
@Service
public class SecurityService {

  @Autowired
  private EmotionRepository emotionRepository;

  @Transactional(readOnly = true)
  public boolean validTrachEmotionRequest(TrachDTO trachDTO, EmotionType type) {

    String uuid = ThreadContext.getUuid();

    Emotion trachEmotion = emotionRepository.findTrachEmotionByUUID(trachDTO.getId(), type, uuid);

    if (trachEmotion != null) {
      return false;
    }

    trachEmotion = emotionRepository.findTrachEmotionByIpAddress(trachDTO.getId(), type, ThreadContext.getIpAddress());

    if (trachEmotion != null) {
      //      return false;
    }

    return true;

  }

  @Transactional(readOnly = true)
  public boolean validCommentEmotionRequest(CommentDTO commentDTO, EmotionType type) {

    String uuid = ThreadContext.getUuid();

    Emotion trachEmotion = emotionRepository.findCommentEmotionByUUID(commentDTO.getId(), type, uuid);

    if (trachEmotion != null) {
      return false;
    }

    trachEmotion = emotionRepository.findCommentEmotionByIpAddress(commentDTO.getId(), type, ThreadContext.getIpAddress());

    if (trachEmotion != null) {
//      return false;
    }

    return true;

  }

}

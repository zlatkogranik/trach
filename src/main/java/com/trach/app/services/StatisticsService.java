package com.trach.app.services;

import com.trach.app.dto.StatisticsDTO;
import com.trach.app.enums.EmotionType;
import com.trach.app.jpa.CommentRepository;
import com.trach.app.jpa.EmotionRepository;
import com.trach.app.jpa.TrachRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticsService {

  @Autowired
  private TrachRepository trachRepository;

  @Autowired
  private CommentRepository commentRepository;

  @Autowired
  private EmotionRepository emotionRepository;

  public StatisticsDTO getStatisticsDTO() {

    Integer countTraches = trachRepository.countApprovedTraches();
    Integer countComments = commentRepository.countApprovedComments();
    Integer countLikes = emotionRepository.countLikesByType(EmotionType.LIKE);
    Integer countShares = trachRepository.countShares();

    StatisticsDTO statisticsDTO = StatisticsDTO.builder()
                                               .traches(countTraches)
                                               .comments(countComments)
                                               .likes(countLikes)
                                               .shares(countShares)
                                               .build();

    return statisticsDTO;
  }

}

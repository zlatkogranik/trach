package com.trach.app.rest.filters;

import com.trach.app.rest.context.ThreadContext;
import com.trach.app.rest.security.JwtToken;
import com.trach.app.rest.security.JwtTokenUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by ToZla on 07/02/2017.
 */
@Log4j2
public class RestApiFilter extends OncePerRequestFilter {

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
    throws ServletException, IOException {

    if (request.getRequestURI().startsWith("/api/")) {

      long currentTimeMillis = System.currentTimeMillis();

      validateHeaders(request);

      ThreadContext.init(request);

      setUniqueToken(request, response);

      log.debug("RequestURI: {}, from ip: {}",
                request.getRequestURI(),
                ThreadContext.get("ipAddress"));

      filterChain.doFilter(request, response);

      log.debug("RequestURI: {}, from ip: {}, took {}ms",
                request.getRequestURI(),
                ThreadContext.get("ipAddress"),
                System.currentTimeMillis() - currentTimeMillis);


      ThreadContext.clear();

    }
    else {

      filterChain.doFilter(request, response);

    }

  }

  private void setUniqueToken(HttpServletRequest request, HttpServletResponse response) {

    if (request.getHeader(ThreadContext.UUID_HEADER) == null && ThreadContext.getUuid() != null) {
      response.addHeader(ThreadContext.UUID_HEADER, ThreadContext.getUuid());
    }

  }

  private void validateHeaders(HttpServletRequest request) {

    String basicUserAuth = request.getHeader("x-tzl");

    Assert.isTrue(basicUserAuth.equalsIgnoreCase("s3cR3t"));

    if (request.getRequestURI().startsWith("/api/admin/")) {

      String token = request.getHeader(JwtTokenUtil.JWT_HEADER);

      JwtToken jwtToken = JwtTokenUtil.parseToken(token);

      Assert.isTrue(JwtTokenUtil.isValid(jwtToken));

    }

  }

  public static void main(String[] args) {
    JwtTokenUtil.generateToken();
  }

}

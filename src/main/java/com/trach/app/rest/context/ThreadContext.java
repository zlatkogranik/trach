package com.trach.app.rest.context;

import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ToZla on 18/02/2017.
 */
@Log4j2
public class ThreadContext {

  public static final String UUID_HEADER = "th-ut";

  private static final String IP_ADDRESS = "ipAddress";
  private static final String UUID = "uuid";

  private static ThreadLocal<Map<String, Object>> threadData = new ThreadLocal<Map<String, Object>>();

  public static void put(String key, Object value) {

    Map<String, Object> map = threadData.get();

    if (map == null) {
      threadData.set(new HashMap<>());
    }

    threadData.get().put(key, value);

  }

  public static Object get(String key) {

    Map<String, Object> map = threadData.get();

    if (map == null) {
      return null;
    }

    return map.get(key);

  }

  public static void clear() {
    threadData.remove();
  }

  public static void init(HttpServletRequest request) {

    String ipAddress = request.getHeader("X-FORWARDED-FOR");

    if (ipAddress == null) {
      ipAddress = request.getRemoteAddr();
    }

    String uuid = request.getHeader(UUID_HEADER);

    if (uuid == null) {
      uuid = java.util.UUID.randomUUID().toString();
    }

    put(IP_ADDRESS, ipAddress);
    put(UUID, uuid);

  }

  public static String getIpAddress() {
    return (String)get(IP_ADDRESS);
  }

  public static String getUuid() {
    return (String)get(UUID);
  }

  public static void setUuid(String value) {
    put(UUID, value);
  }

}

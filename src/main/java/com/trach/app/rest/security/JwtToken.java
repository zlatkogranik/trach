package com.trach.app.rest.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by ToZla on 29/09/2016.
 */
@Builder
@AllArgsConstructor
public class JwtToken {

    @Getter @Setter
    private String user;

    @Getter @Setter
    private String domain;

}

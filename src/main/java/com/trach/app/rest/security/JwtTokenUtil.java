package com.trach.app.rest.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Created by ToZla on 29/09/2016.
 */
public class JwtTokenUtil {

  public static final String JWT_HEADER = "xs";

  private static final String JWT_USER = "ToZL@";
  private static final String JWT_DOMAIN = "H3mij@";
  private static final String JWT_SECRET = "jwtS3cr3t";

  /**
   * Tries to parse specified String as a JWT token. If successful, returns JwtToken object with userId and domain prefilled (extracted from token).
   * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
   *
   * @param token the JWT token to parse
   * @return the JwtToken object extracted from specified token or null if a token is invalid.
   */
  public static JwtToken parseToken(String token) {

    try {
      Claims body = Jwts.parser()
                        .setSigningKey(JWT_SECRET)
                        .parseClaimsJws(token)
                        .getBody();

      JwtToken jwtToken = JwtToken.builder()
                                  .user((String)body.get("user"))
                                  .domain((String)body.get("domain"))
                                  .build();

      return jwtToken;

    }
    catch (Exception e) {
      return null;
    }
  }

  /**
   * Generates a JWT token containing username as subject, and userId and role as additional claims. These properties are taken from the specified
   * User object. Tokens validity is infinite.
   *
   * @return the JWT tokens
   */
  public static String generateToken() {

    Claims claims = Jwts.claims();
    claims.put("user", "ToZL@");
    claims.put("domain", "H3mij@");

    return Jwts.builder()
               .setClaims(claims)
               .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
               .compact();
  }

  public static boolean isValid(JwtToken jwtToken) {
    return jwtToken.getUser().equals(JWT_USER) && jwtToken.getDomain().equals(JWT_DOMAIN);
  }

}

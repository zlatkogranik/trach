package com.trach.app.rest.controllers;

import com.trach.app.dto.StatisticsDTO;
import com.trach.app.services.StatisticsService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by ToZla on 18/02/2017.
 */
@Log4j2
@Controller
@RequestMapping("/api/statistics")
public class StatisticsRestController {

  @Autowired
  private StatisticsService statisticsService;

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity<StatisticsDTO> getStatistics() {

    StatisticsDTO statisticsDTO = statisticsService.getStatisticsDTO();

    return new ResponseEntity<>(statisticsDTO, HttpStatus.OK);
  }

}

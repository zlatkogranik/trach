package com.trach.app.rest.controllers.admin;

import com.trach.app.dto.CommentDTO;
import com.trach.app.dto.FilterMetadataDTO;
import com.trach.app.dto.PageDTO;
import com.trach.app.enums.FilterMatchMode;
import com.trach.app.rest.controllers.base.Headers;
import com.trach.app.services.CommentService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Log4j2
@Controller
@RequestMapping("/api/admin/comment")
public class AdminCommentRestController {

  @Autowired
  private CommentService commentService;

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, value = "/pageable")
  public ResponseEntity<Page<CommentDTO>> getCommentsPageable(@RequestBody PageDTO pageDTO) {

    FilterMetadataDTO filterMetadata = FilterMetadataDTO.builder()
                                                        .property("approved")
                                                        .value("false")
                                                        .matchMode(FilterMatchMode.EQUAL)
                                                        .build();

    pageDTO.addFilterMetaData(filterMetadata);

    Page<CommentDTO> commentDTOsPage = commentService.findCommentDTOsPageable(pageDTO);

    return new ResponseEntity<>(commentDTOsPage, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.PUT, path = "/approve")
  public ResponseEntity<CommentDTO> approve(@RequestBody CommentDTO commentDTO) {

    CommentDTO approvedComment = commentService.approve(commentDTO);

    HttpHeaders headers = new HttpHeaders();
    headers.set(Headers.SUCCESS_MESSAGE, "Comment has been approved!");

    return new ResponseEntity<>(approvedComment, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.PUT, path = "/reject")
  public ResponseEntity<CommentDTO> reject(@RequestBody CommentDTO commentDTO) {

    CommentDTO rejectedComment = commentService.reject(commentDTO);

    HttpHeaders headers = new HttpHeaders();
    headers.set(Headers.SUCCESS_MESSAGE, "Comment has been rejected!");

    return new ResponseEntity<>(rejectedComment, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.PUT)
  public ResponseEntity<CommentDTO> update(@RequestBody CommentDTO commentDTO) {

    CommentDTO updatedComment = commentService.update(commentDTO);

    HttpHeaders headers = new HttpHeaders();
    headers.set(Headers.SUCCESS_MESSAGE, "Comment has been updated!");

    return new ResponseEntity<>(updatedComment, headers, HttpStatus.OK);
  }

}

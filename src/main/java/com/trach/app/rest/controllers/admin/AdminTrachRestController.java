package com.trach.app.rest.controllers.admin;

import com.trach.app.dto.FilterMetadataDTO;
import com.trach.app.dto.PageDTO;
import com.trach.app.dto.TrachDTO;
import com.trach.app.dto.TrachDTOAdvance;
import com.trach.app.enums.FilterMatchMode;
import com.trach.app.rest.controllers.base.Headers;
import com.trach.app.services.TrachService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Log4j2
@Controller
@RequestMapping("/api/admin/trach")
public class AdminTrachRestController {

  @Autowired
  private TrachService trachService;

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, value = "/pageable")
  public ResponseEntity<Page<TrachDTOAdvance>> getTrachesPageable(@RequestBody PageDTO pageDTO) {

    FilterMetadataDTO filterMetadata = FilterMetadataDTO.builder()
                                                        .property("approved")
                                                        .value("false")
                                                        .matchMode(FilterMatchMode.EQUAL)
                                                        .build();

    pageDTO.addFilterMetaData(filterMetadata);

    Page<TrachDTOAdvance> trachDTOsPage = trachService.findTrachDTOsPageable(pageDTO);

    return new ResponseEntity<>(trachDTOsPage, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.PUT, path = "/approve")
  public ResponseEntity<TrachDTOAdvance> approve(@RequestBody TrachDTO trachDTO) {

    TrachDTOAdvance approvedTrach = trachService.approve(trachDTO);

    HttpHeaders headers = new HttpHeaders();
    headers.set(Headers.SUCCESS_MESSAGE, "Trach has been approved!");

    return new ResponseEntity<>(approvedTrach, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.PUT, path = "/reject")
  public ResponseEntity<TrachDTO> reject(@RequestBody TrachDTO trachDTO) {

    TrachDTO deleteTrach = trachService.reject(trachDTO);

    HttpHeaders headers = new HttpHeaders();
    headers.set(Headers.SUCCESS_MESSAGE, "Trach has been rejected!");

    return new ResponseEntity<>(deleteTrach, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.PUT)
  public ResponseEntity<TrachDTOAdvance> update(@RequestBody TrachDTO trachDTO) {

    TrachDTOAdvance updatedTrach = trachService.saveTrach(trachDTO);

    HttpHeaders headers = new HttpHeaders();
    headers.set(Headers.SUCCESS_MESSAGE, "Trach has been updated!");

    return new ResponseEntity<>(updatedTrach, headers, HttpStatus.OK);
  }

}

package com.trach.app.rest.controllers;

import com.trach.app.dto.TagDTO;
import com.trach.app.services.TagService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Log4j2
@Controller
@RequestMapping("/api/tag")
public class TagRestController {

  @Autowired
  private TagService tagService;

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.GET, value = "/{id}")
  public ResponseEntity<TagDTO> getTagById(@PathVariable("id") Integer id) {

    TagDTO tagDTO = tagService.findTagById(id);

    return new ResponseEntity<>(tagDTO, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, value = "/text")
  public ResponseEntity<TagDTO> getTagByText(@RequestBody String text) {

    TagDTO trachDTO = tagService.findTagsByText(text);

    return new ResponseEntity<>(trachDTO, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<Page<TagDTO>> getTagsPageableByPhrase(@RequestBody String phrase) {

    Page<TagDTO> trachDTOsPage = tagService.findTagsByPhrase(phrase);

    return new ResponseEntity<>(trachDTOsPage, HttpStatus.OK);
  }

}

package com.trach.app.rest.controllers;

import com.trach.app.dto.CommentDTO;
import com.trach.app.rest.controllers.base.Headers;
import com.trach.app.services.CommentService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@Controller
@RequestMapping("/api/comment")
public class CommentRestController {

  @Autowired
  private CommentService commentService;

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.GET, value = "/{id}")
  public ResponseEntity<CommentDTO> getCommentById(@PathVariable("id") Integer id) {

    CommentDTO commentDTO = commentService.findCommentsById(id);

    return new ResponseEntity<>(commentDTO, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.GET, value = "/trach/{id}")
  public ResponseEntity<List<CommentDTO>> getCommentByTrachId(@PathVariable("id") Integer id) {

    List<CommentDTO> commentDTOS = commentService.findCommentsByTrachId(id);

    return new ResponseEntity<>(commentDTOS, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, value = "/{id}")
  public ResponseEntity<CommentDTO> saveCommentByTrachId(@PathVariable("id") Integer trachId, @RequestBody CommentDTO comment) {

    CommentDTO commentDTO = commentService.saveCommentByTrachId(trachId, comment);

    HttpHeaders headers = new HttpHeaders();
    headers.set(Headers.SUCCESS_MESSAGE, "Your comment has been sent for approval!");

    return new ResponseEntity<>(commentDTO, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, path = "like")
  public ResponseEntity<CommentDTO> like(@RequestBody CommentDTO commentDTO) {

    commentService.like(commentDTO);

    HttpHeaders headers = new HttpHeaders();

    return new ResponseEntity<>(commentDTO, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, path = "unlike")
  public ResponseEntity<CommentDTO> unlike(@RequestBody CommentDTO commentDTO) {

    commentService.unlike(commentDTO);

    HttpHeaders headers = new HttpHeaders();

    return new ResponseEntity<>(commentDTO, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.PUT)
  public ResponseEntity<CommentDTO> approve(@RequestBody CommentDTO commentDTO) {

    CommentDTO approvedComment = commentService.approve(commentDTO);

    HttpHeaders headers = new HttpHeaders();
    headers.set(Headers.SUCCESS_MESSAGE, "Comment has been approved!");

    return new ResponseEntity<>(approvedComment, headers, HttpStatus.OK);
  }

}

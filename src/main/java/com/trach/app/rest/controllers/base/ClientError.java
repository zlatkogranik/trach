package com.trach.app.rest.controllers.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by ToZla on 05/10/2016.
 */
@AllArgsConstructor
public class ClientError {

    private @Getter @Setter String error;

    private @Getter @Setter String requestedURI;

}

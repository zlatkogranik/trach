package com.trach.app.rest.controllers.base;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ToZla on 27/10/2016.
 */
@Log4j2
@ControllerAdvice
public class ExceptionControllerAdvice {

    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ClientError> errorHandler(HttpServletRequest request, Exception exception) {

        log.error("Error occurred: {}, requestedURI: {}.", exception.getMessage(), request.getRequestURI(), exception);

        HttpHeaders headers = new HttpHeaders();
        headers.set(Headers.ERROR_MESSAGE, exception.getMessage());

        ClientError clientError = new ClientError(exception.getMessage(), request.getRequestURI());

        return new ResponseEntity<>(clientError, headers, HttpStatus.BAD_REQUEST);

    }

}

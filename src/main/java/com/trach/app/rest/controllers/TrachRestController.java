package com.trach.app.rest.controllers;

import com.trach.app.dto.FilterMetadataDTO;
import com.trach.app.dto.PageDTO;
import com.trach.app.dto.TrachDTO;
import com.trach.app.dto.TrachDTOAdvance;
import com.trach.app.enums.FilterMatchMode;
import com.trach.app.rest.controllers.base.Headers;
import com.trach.app.services.TrachService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Log4j2
@Controller
@RequestMapping("/api/trach")
public class TrachRestController {

  @Autowired
  private TrachService trachService;

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.GET, value = "/{id}")
  public ResponseEntity<TrachDTO> getTrachById(@PathVariable("id") Integer id) {

    TrachDTOAdvance trachById = trachService.findTrachById(id);

    return new ResponseEntity<>(trachById, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, value = "/pageable")
  public ResponseEntity<Page<TrachDTOAdvance>> getTrachesPageable(@RequestBody PageDTO pageDTO) {

    FilterMetadataDTO filterMetadata = FilterMetadataDTO.builder()
                                                        .property("approved")
                                                        .value("true")
                                                        .matchMode(FilterMatchMode.EQUAL)
                                                        .build();

    pageDTO.addFilterMetaData(filterMetadata);

    Page<TrachDTOAdvance> trachDTOsPage = trachService.findTrachDTOsPageable(pageDTO);

    return new ResponseEntity<>(trachDTOsPage, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity<TrachDTOAdvance> save(@RequestBody TrachDTO trachDTO) {

    TrachDTOAdvance savedTrachDTO = trachService.saveTrach(trachDTO);

    HttpHeaders headers = new HttpHeaders();
    headers.set(Headers.SUCCESS_MESSAGE, "Your trach has been sent for approval!");

    return new ResponseEntity<>(savedTrachDTO, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, path = "/like")
  public ResponseEntity<TrachDTO> like(@RequestBody TrachDTO trachDTO) {

    trachService.like(trachDTO);

    HttpHeaders headers = new HttpHeaders();

    return new ResponseEntity<>(trachDTO, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, path = "/unlike")
  public ResponseEntity<TrachDTO> unlike(@RequestBody TrachDTO trachDTO) {

    trachService.unlike(trachDTO);

    HttpHeaders headers = new HttpHeaders();

    return new ResponseEntity<>(trachDTO, headers, HttpStatus.OK);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @RequestMapping(method = RequestMethod.POST, path = "/share")
  public ResponseEntity<TrachDTO> share(@RequestBody TrachDTO trachDTO) {

    trachService.share(trachDTO);

    HttpHeaders headers = new HttpHeaders();

    return new ResponseEntity<>(trachDTO, headers, HttpStatus.OK);
  }

}

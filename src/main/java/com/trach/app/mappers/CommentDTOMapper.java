package com.trach.app.mappers;

import com.trach.app.dto.CommentDTO;
import com.trach.app.mappers.config.BasicElementMapper;
import com.trach.app.model.Comment;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;

/**
 * Created by ToZla on 12/10/2016.
 */
@Mapper
public interface CommentDTOMapper {

  CommentDTOMapper INSTANCE = Mappers.getMapper(CommentDTOMapper.class);

  @BasicElementMapper
  @Mapping(target = "id", source = "id")
  @Mapping(target = "date", source = "date")
  @Mapping(target = "text", source = "text")
  @Mapping(target = "likes", source = "likes")
  @Mapping(target = "unlikes", source = "unlikes")
  @Mapping(target = "author", source = "author")
  public CommentDTO map(Comment comment);

  @InheritConfiguration(name = "map")
  public void map(CommentDTO commentDTO, @MappingTarget Comment comment);

  @IterableMapping(qualifiedBy = BasicElementMapper.class)
  public Set<CommentDTO> mapComments(Set<Comment> comments);

  @IterableMapping(qualifiedBy = BasicElementMapper.class)
  public List<CommentDTO> mapComments(List<Comment> comments);

}

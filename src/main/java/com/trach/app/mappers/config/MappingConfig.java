package com.trach.app.mappers.config;

import org.mapstruct.MapperConfig;
import org.mapstruct.MappingInheritanceStrategy;
import org.mapstruct.ReportingPolicy;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ToZla on 18/11/2016.
 */
@MapperConfig(
  unmappedTargetPolicy = ReportingPolicy.ERROR,
  mappingInheritanceStrategy = MappingInheritanceStrategy.AUTO_INHERIT_FROM_CONFIG
)
public interface MappingConfig {

  static String formatDate(Date date) {
    SimpleDateFormat df = new SimpleDateFormat("dd-mm-yyyy");
    return df.format(date);
  }

}


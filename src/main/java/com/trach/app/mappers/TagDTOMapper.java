package com.trach.app.mappers;

import com.trach.app.dto.TagDTO;
import com.trach.app.mappers.config.BasicElementMapper;
import com.trach.app.model.Tag;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Created by ToZla on 12/10/2016.
 */
@Mapper
public interface TagDTOMapper {

    TagDTOMapper INSTANCE = Mappers.getMapper(TagDTOMapper.class);

    @BasicElementMapper
    @Mapping(source = "id", target = "id")
    @Mapping(source = "text", target = "text")
    public TagDTO map(Tag tag);

    @IterableMapping(qualifiedBy = BasicElementMapper.class)
    public List<TagDTO> mapTags(List<Tag> tags);

}

package com.trach.app.mappers;

import com.trach.app.dto.TagDTO;
import com.trach.app.dto.TrachDTO;
import com.trach.app.dto.TrachDTOAdvance;
import com.trach.app.mappers.config.AdvanceElementMapper;
import com.trach.app.mappers.config.BasicElementMapper;
import com.trach.app.model.Trach;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Created by ToZla on 12/10/2016.
 */
@Mapper
public interface TrachDTOMapper {

  TrachDTOMapper INSTANCE = Mappers.getMapper(TrachDTOMapper.class);

  @BasicElementMapper
  @Mapping(source = "id", target = "id")
  @Mapping(source = "text", target = "text")
  @Mapping(source = "link", target = "link")
  @Mapping(source = "likes", target = "likes")
  @Mapping(source = "unlikes", target = "unlikes")
  @Mapping(source = "shares", target = "shares")
  @Mapping(source = "comment", target = "comment")
  public TrachDTO map(Trach trach);

  @InheritConfiguration(name = "map")
  @Mapping(target = "tags", ignore = true)
  @Mapping(target = "comments", ignore = true)
  public void map(TrachDTO trachDTO, @MappingTarget Trach trach);

  @AdvanceElementMapper
  @InheritConfiguration(name = "map")
  @Mapping(target = "tags", expression = "java(TrachDTOMapper.getTags(trach))")
  @Mapping(target = "comments", ignore = true)
  public TrachDTOAdvance mapAdvance(Trach trach);

  @IterableMapping(qualifiedBy = AdvanceElementMapper.class)
  public List<TrachDTOAdvance> mapAdvanceList(List<Trach> trachs);

  public static List<TagDTO> getTags(Trach trach) {
    return trach.getTags() != null ? TagDTOMapper.INSTANCE.mapTags(trach.getTags()) : null;
  }

}

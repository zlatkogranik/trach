package com.trach.app.model;

import com.trach.app.enums.EmotionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;

import javax.persistence.*;

/**
 * Created by ToZla on 18/02/2017.
 */
@Entity
@Builder
@ToString
@AllArgsConstructor
@Table(name = "EMOTION")
public class Emotion {

  public Emotion() {

  }

  @Id
  @GeneratedValue
  private Integer id;

  @Column(name = "trach_id")
  private Integer trachId;

  @Column(name = "comment_id")
  private Integer commentId;

  @Column(name = "emotion")
  @Enumerated(EnumType.STRING)
  private EmotionType type;

  @Column(name = "ip_address")
  private String ipAddress;

  @Column(name = "uuid")
  private String uuid;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getTrachId() {
    return trachId;
  }

  public void setTrachId(Integer trachId) {
    this.trachId = trachId;
  }

  public Integer getCommentId() {
    return commentId;
  }

  public void setCommentId(Integer commentId) {
    this.commentId = commentId;
  }

  public EmotionType getType() {
    return type;
  }

  public void setType(EmotionType type) {
    this.type = type;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }
}

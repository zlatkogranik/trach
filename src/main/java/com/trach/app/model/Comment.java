package com.trach.app.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Builder
@ToString
@AllArgsConstructor
@Table(name = "COMMENT")
public class Comment {

  public Comment() {

  }

  @Id
  @GeneratedValue
  private Integer id;

  @Column(name = "date")
  private Date date = new Date();

  @Column(name = "text", length = 1000)
  private String text;

  @Column(name = "author")
  private String author;

  @Column(name = "like_count")
  private Integer likes = 0;

  @Column(name = "unlike_count")
  private Integer unlikes = 0;

  @Column(name = "ip_address")
  private String ipAddress;

  @Column(name = "uuid")
  private String uuid;

  @Column(name = "approved")
  private boolean approved;

  @ManyToOne
  private Trach trach;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public Integer getLikes() {
    return likes;
  }

  public void setLikes(Integer likes) {
    this.likes = likes;
  }

  public Integer getUnlikes() {
    return unlikes;
  }

  public void setUnlikes(Integer unlikes) {
    this.unlikes = unlikes;
  }

  public Trach getTrach() {
    return trach;
  }

  public void setTrach(Trach trach) {
    this.trach = trach;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public boolean isApproved() {
    return approved;
  }

  public void setApproved(boolean approved) {
    this.approved = approved;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }
}

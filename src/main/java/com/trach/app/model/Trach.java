package com.trach.app.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Builder
@ToString
@AllArgsConstructor
@Table(name = "TRACH")
public class Trach {

  public Trach() {

  }

  @Id
  @GeneratedValue
  private Integer id;

  @Column(name = "date")
  private Date date = new Date();

  @Column(name = "text", length = 3000)
  private String text;

  @Column(name = "link")
  private String link;

  @Column(name = "like_count")
  private Integer likes = 0;

  @Column(name = "unlike_count")
  private Integer unlikes = 0;

  @Column(name = "share_count")
  private Integer shares = 0;

  @Column(name = "comment_count")
  private Integer comment = 0;

  @Column(name = "approved")
  private boolean approved = false;

  @Column(name = "ip_address")
  private String ipAddress;

  @Column(name = "uuid")
  private String uuid;

  @ManyToMany
  private List<Tag> tags;

  @OneToMany(mappedBy = "trach", fetch = FetchType.LAZY)
  private List<Comment> comments;

  public Integer getId() {
    return this.id;
  }

  public Date getDate() {
    return this.date;
  }

  public String getText() {
    return this.text;
  }

  public String getLink() {
    return this.link;
  }

  public Integer getLikes() {
    return this.likes;
  }

  public Integer getUnlikes() {
    return this.unlikes;
  }

  public Integer getShares() {
    return this.shares;
  }

  public Integer getComment() {
    return this.comment;
  }

  public boolean isApproved() {
    return this.approved;
  }

  public List<Tag> getTags() {
    return this.tags;
  }

  public List<Comment> getComments() {
    return this.comments;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public void setText(String text) {
    this.text = text;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public void setLikes(Integer likes) {
    this.likes = likes;
  }

  public void setUnlikes(Integer unlikes) {
    this.unlikes = unlikes;
  }

  public void setShares(Integer shares) {
    this.shares = shares;
  }

  public void setComment(Integer comment) {
    this.comment = comment;
  }

  public void setApproved(boolean approved) {
    this.approved = approved;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }

  public void setComments(List<Comment> comments) {
    this.comments = comments;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }
}

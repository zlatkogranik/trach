package com.trach.app.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Builder
@ToString
@AllArgsConstructor
@Table(name = "TAG")
public class Tag {

  public Tag() {

  }

  @Id
  @GeneratedValue
  private Integer id;

  @Column(name = "text", unique = true)
  private String text;

  @Column(name = "count")
  private Integer count = 0;

  public Integer getId() {
    return this.id;
  }

  public String getText() {
    return this.text;
  }

  public Integer getCount() {
    return this.count;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setText(String text) {
    this.text = text;
  }

  public void setCount(Integer count) {
    this.count = count;
  }
}

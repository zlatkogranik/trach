package com.trach.app.dto;

/**
 * Created by ToZla on 07/02/2017.
 */
public class TagDTO {

  private Integer id;
  private String text;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

}

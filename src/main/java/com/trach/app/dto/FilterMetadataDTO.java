package com.trach.app.dto;

import com.trach.app.enums.FilterMatchMode;
import lombok.AllArgsConstructor;
import lombok.Builder;

/**
 * Created by ToZla on 17/10/2016.
 */
@Builder
@AllArgsConstructor
public class FilterMetadataDTO {

  private String value;
  private String property;
  private FilterMatchMode matchMode;

  public FilterMetadataDTO() {

  }

  public String getValue() {
    return this.value;
  }

  public String getProperty() {
    return this.property;
  }

  public FilterMatchMode getMatchMode() {
    return this.matchMode;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public void setProperty(String property) {
    this.property = property;
  }

  public void setMatchMode(FilterMatchMode matchMode) {
    this.matchMode = matchMode;
  }

}

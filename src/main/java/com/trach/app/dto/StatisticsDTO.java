package com.trach.app.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by ToZla on 18/02/2017.
 */
@Getter
@Setter
@Builder
public class StatisticsDTO {

  private Integer traches;
  private Integer likes;
  private Integer comments;
  private Integer shares;

}

package com.trach.app.dto;

import java.util.List;

/**
 * Created by ToZla on 06/02/2017.
 */
public class TrachDTOAdvance extends TrachDTO {

  private List<TagDTO> tags;
  private List<CommentDTO> comments;

  public List<TagDTO> getTags() {
    return this.tags;
  }

  public List<CommentDTO> getComments() {
    return this.comments;
  }

  public void setTags(List<TagDTO> tags) {
    this.tags = tags;
  }

  public void setComments(List<CommentDTO> comments) {
    this.comments = comments;
  }
}

package com.trach.app.dto;

import java.util.Date;

/**
 * Created by ToZla on 09/02/2017.
 */
public class CommentDTO {

  private Integer id;
  private Date date;
  private String text;
  private String author;
  private Integer likes;
  private Integer unlikes;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public Integer getLikes() {
    return likes;
  }

  public void setLikes(Integer likes) {
    this.likes = likes;
  }

  public Integer getUnlikes() {
    return unlikes;
  }

  public void setUnlikes(Integer unlikes) {
    this.unlikes = unlikes;
  }
}

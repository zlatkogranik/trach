package com.trach.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ToZla on 06/02/2017.
 */
@Getter
@Setter
public class PageDTO {

  private Integer firstResult;
  private Integer maxResult;
  private List<String> tags;
  private String sortField;
  private Integer sortOrder;
  private List<FilterMetadataDTO> filterMetadata;

  public void addFilterMetaData(FilterMetadataDTO filterMetadataDTO) {
    if (this.filterMetadata == null) {
      this.filterMetadata = new ArrayList<>();
    }
    this.filterMetadata.add(filterMetadataDTO);
  }

}

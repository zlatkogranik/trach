package com.trach.app.dto;

import java.util.Date;

/**
 * Created by ToZla on 06/02/2017.
 */
public class TrachDTO {

  private Integer id;
  private String text;
  private String link;
  private Date date;
  private Integer likes;
  private Integer unlikes;
  private Integer shares;
  private Integer comment;

  public Integer getId() {
    return this.id;
  }

  public String getText() {
    return this.text;
  }

  public String getLink() {
    return this.link;
  }

  public Date getDate() {
    return this.date;
  }

  public Integer getLikes() {
    return this.likes;
  }

  public Integer getUnlikes() {
    return this.unlikes;
  }

  public Integer getShares() {
    return this.shares;
  }

  public Integer getComment() {
    return this.comment;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setText(String text) {
    this.text = text;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public void setLikes(Integer likes) {
    this.likes = likes;
  }

  public void setUnlikes(Integer unlikes) {
    this.unlikes = unlikes;
  }

  public void setShares(Integer shares) {
    this.shares = shares;
  }

  public void setComment(Integer comment) {
    this.comment = comment;
  }
}

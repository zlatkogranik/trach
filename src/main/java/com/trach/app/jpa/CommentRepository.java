package com.trach.app.jpa;

import com.trach.app.jpa.custom.CommentRepositoryCustom;
import com.trach.app.model.Comment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface CommentRepository extends Repository<Comment, Long>, CommentRepositoryCustom {

  public Comment findCommentById(Integer id);

  @Query("select c from Comment c where c.trach.id = ?1 and c.approved = true")
  public List<Comment> findCommentsByTrachId(Integer id);

  @Modifying
  @Query("update Comment c set c.likes = c.likes + 1 where c.id = ?1")
  public void increaseLike(Integer id);

  @Modifying
  @Query("update Comment c set c.unlikes = c.unlikes + 1 where c.id = ?1")
  public void increaseUnlike(Integer id);

  @Modifying
  @Query("update Comment c set c.likes = c.likes - 1 where c.id = ?1")
  public void decreaseLike(Integer id);

  @Modifying
  @Query("update Comment c set c.unlikes = c.unlikes - 1 where c.id = ?1")
  public void decreaseUnlike(Integer id);

  @Query("select count(c) from Comment c where c.approved = true")
  public Integer countApprovedComments();

}

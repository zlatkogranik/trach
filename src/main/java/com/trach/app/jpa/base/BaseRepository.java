package com.trach.app.jpa.base;

import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;

/**
 * Created by ToZla on 08/09/2016.
 */
@NoRepositoryBean
public interface BaseRepository<T> {

    public void save(T o);

    public void update(T o);

    public void delete(T o);

    public void saveOrUpdate(T o);

    public void flush();

    public void clear();

    public void evict(T o);

    public T merge(T o);

    public List<T> loadAll(Class<T> clazz, Collection<Integer> ids);

    public default <T> T getSingleResultOrNull(Query query, Class<T> type) {
        List<T> results = (List<T>)query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        else if (results.size() == 1) {
            return (T)results.get(0);
        }
        throw new NonUniqueResultException();
    }

}

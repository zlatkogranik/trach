package com.trach.app.jpa.base;

import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ToZla on 08/09/2016.
 */
@NoRepositoryBean
public class BaseRepositoryImpl<T> implements BaseRepository<T> {

    @PersistenceContext()
    protected EntityManager em;

    public void save(T o) {
        em.persist(o);
    }

    public void update(T o) {
        em.merge(o);
    }

    public void delete(T o) {
        em.remove(em.contains(o) ? o : em.merge(o));
    }

    public void saveOrUpdate(T o) {
        em.merge(o);
    }

    public void flush() {
        em.flush();
    }

    public void clear() {
        em.clear();
    }

    public T merge(T o) {
        return em.merge(o);
    }

    public void evict(T o) {
        em.detach(o);
    }

    @SuppressWarnings("unchecked")
    public List<T> loadAll(Class<T> clazz, Collection<Integer> ids) {
        List<T> result = new ArrayList<T>();
        if (ids != null) {
            for (Integer id : ids) {
                result.add((T)em.find(clazz, id));
            }
        }
        return result;
    }

}

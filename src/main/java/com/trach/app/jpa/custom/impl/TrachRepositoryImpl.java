package com.trach.app.jpa.custom.impl;

import com.trach.app.dto.PageDTO;
import com.trach.app.jpa.base.BaseRepositoryImpl;
import com.trach.app.jpa.custom.TrachRepositoryCustom;
import com.trach.app.jpa.util.PageUtil;
import com.trach.app.jpa.util.QueryBuilder;
import com.trach.app.model.Trach;
import org.springframework.data.domain.Page;

import javax.persistence.Query;

/**
 * Created by ToZla on 15/02/2017.
 */
public class TrachRepositoryImpl extends BaseRepositoryImpl<Trach> implements TrachRepositoryCustom {

  @Override
  public Page<Trach> findTrachesPageable(PageDTO pageDTO) {

    StringBuilder hqlBuilder = new StringBuilder().append("select distinct(s) from Trach s");
    StringBuilder countHqlBuilder = new StringBuilder().append("select count(s) from Trach s");

    QueryBuilder.where(pageDTO, hqlBuilder, countHqlBuilder);

    QueryBuilder.orderBy(pageDTO, hqlBuilder, "s.date");

    String hql = hqlBuilder.toString();
    String countHql = countHqlBuilder.toString();

    Query query = em.createQuery(hql, Trach.class);
    Query countQuery = em.createQuery(countHql, Long.class);

    return PageUtil.getPage(query, countQuery, pageDTO.getFirstResult(), pageDTO.getMaxResult(), Trach.class);

  }
}

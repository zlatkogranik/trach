package com.trach.app.jpa.custom.impl;

import com.trach.app.jpa.base.BaseRepositoryImpl;
import com.trach.app.jpa.custom.EmotionRepositoryCustom;
import com.trach.app.model.Emotion;

/**
 * Created by ToZla on 15/02/2017.
 */
public class EmotionRepositoryImpl extends BaseRepositoryImpl<Emotion> implements EmotionRepositoryCustom {

}

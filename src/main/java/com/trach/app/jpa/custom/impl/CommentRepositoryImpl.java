package com.trach.app.jpa.custom.impl;

import com.trach.app.dto.PageDTO;
import com.trach.app.jpa.base.BaseRepositoryImpl;
import com.trach.app.jpa.custom.CommentRepositoryCustom;
import com.trach.app.jpa.util.PageUtil;
import com.trach.app.jpa.util.QueryBuilder;
import com.trach.app.model.Comment;
import org.springframework.data.domain.Page;

import javax.persistence.Query;

/**
 * Created by ToZla on 15/02/2017.
 */
public class CommentRepositoryImpl extends BaseRepositoryImpl<Comment> implements CommentRepositoryCustom {

  @Override
  public Page<Comment> findCommentsPageable(PageDTO pageDTO) {

    StringBuilder hqlBuilder = new StringBuilder().append("select distinct(s) from Comment s");
    StringBuilder countHqlBuilder = new StringBuilder().append("select count(s) from Comment s");

    QueryBuilder.where(pageDTO, hqlBuilder, countHqlBuilder);

    QueryBuilder.orderBy(pageDTO, hqlBuilder, "s.date");

    String hql = hqlBuilder.toString();
    String countHql = countHqlBuilder.toString();

    Query query = em.createQuery(hql, Comment.class);
    Query countQuery = em.createQuery(countHql, Long.class);

    return PageUtil.getPage(query, countQuery, pageDTO.getFirstResult(), pageDTO.getMaxResult(), Comment.class);

  }

}

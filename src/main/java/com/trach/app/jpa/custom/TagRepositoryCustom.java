package com.trach.app.jpa.custom;

import com.trach.app.jpa.base.BaseRepository;
import com.trach.app.model.Tag;

/**
 * Created by ToZla on 15/02/2017.
 */
public interface TagRepositoryCustom extends BaseRepository<Tag> {

}

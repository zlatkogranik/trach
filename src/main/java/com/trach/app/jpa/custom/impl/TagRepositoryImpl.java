package com.trach.app.jpa.custom.impl;

import com.trach.app.jpa.base.BaseRepositoryImpl;
import com.trach.app.jpa.custom.TagRepositoryCustom;
import com.trach.app.model.Tag;

/**
 * Created by ToZla on 15/02/2017.
 */
public class TagRepositoryImpl extends BaseRepositoryImpl<Tag> implements TagRepositoryCustom {

}

package com.trach.app.jpa.custom;

import com.trach.app.dto.PageDTO;
import com.trach.app.jpa.base.BaseRepository;
import com.trach.app.model.Trach;
import org.springframework.data.domain.Page;

/**
 * Created by ToZla on 15/02/2017.
 */
public interface TrachRepositoryCustom extends BaseRepository<Trach> {

  public Page<Trach> findTrachesPageable(PageDTO pageDTO);

}

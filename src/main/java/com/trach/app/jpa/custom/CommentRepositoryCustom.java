package com.trach.app.jpa.custom;

import com.trach.app.dto.PageDTO;
import com.trach.app.jpa.base.BaseRepository;
import com.trach.app.model.Comment;
import org.springframework.data.domain.Page;

/**
 * Created by ToZla on 15/02/2017.
 */
public interface CommentRepositoryCustom extends BaseRepository<Comment> {

  public Page<Comment> findCommentsPageable(PageDTO pageDTO);

}

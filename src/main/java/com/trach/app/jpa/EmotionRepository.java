package com.trach.app.jpa;

import com.trach.app.enums.EmotionType;
import com.trach.app.jpa.custom.EmotionRepositoryCustom;
import com.trach.app.model.Emotion;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface EmotionRepository extends Repository<Emotion, Long>, EmotionRepositoryCustom {

  @Query("select e from Emotion e where e.trachId = ?1 and e.type = ?2")
  public Emotion findTrachEmotion(Integer trachId, EmotionType emotionType);

  @Query("select e from Emotion e where e.commentId = ?1 and e.type = ?2")
  public Emotion findCommentEmotion(Integer commentId, EmotionType emotionType);

  @Query("select e from Emotion e where e.trachId = ?1 and e.type = ?2 and e.commentId is null and e.ipAddress = ?3")
  public Emotion findTrachEmotionByIpAddress(Integer trachId, EmotionType emotionType, String ipAddress);

  @Query("select e from Emotion e where e.commentId = ?1 and e.type = ?2 and e.ipAddress = ?3")
  public Emotion findCommentEmotionByIpAddress(Integer commentId, EmotionType emotionType, String ipAddress);

  @Query("select e from Emotion e where e.trachId = ?1 and e.type = ?2 and e.commentId is null and e.uuid = ?3")
  public Emotion findTrachEmotionByUUID(Integer trachId, EmotionType emotionType, String uuid);

  @Query("select e from Emotion e where e.commentId = ?1 and e.type = ?2 and e.uuid = ?3")
  public Emotion findCommentEmotionByUUID(Integer commentId, EmotionType emotionType, String uuid);

  @Modifying
  @Query("delete from Emotion e where e.trachId = ?1")
  public void deleteEmotionByTrachId(Integer trachId);

  @Modifying
  @Query("delete from Emotion e where e.id = ?1")
  public void deleteEmotionById(Integer id);

  @Query("select count(e) from Emotion e where e.type = ?1")
  public Integer countLikesByType(EmotionType type);

}

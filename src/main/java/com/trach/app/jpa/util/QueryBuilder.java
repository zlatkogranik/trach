package com.trach.app.jpa.util;

import com.trach.app.dto.FilterMetadataDTO;
import com.trach.app.dto.PageDTO;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ToZla on 20/12/2016.
 */
public class QueryBuilder {

  public static void where(PageDTO pageDTO, StringBuilder hqlBuilder, StringBuilder countHqlBuilder) {

    if (pageDTO.getTags() != null) {
      for (String tag : pageDTO.getTags()) {
        hqlBuilder.append(" join s.tags t");
        countHqlBuilder.append(" join s.tags t ");
      }
    }

    hqlBuilder.append(" where 1=1 ");
    countHqlBuilder.append(" where 1=1 ");

    if (pageDTO.getTags() != null) {
      for (String tag : pageDTO.getTags()) {
        hqlBuilder.append(" and t.id in (" + tag + ") ");
        countHqlBuilder.append(" and t.id in (" + tag + ") ");
      }
    }

    if (pageDTO.getFilterMetadata() != null) {
      for (FilterMetadataDTO filterMetadataDTO : pageDTO.getFilterMetadata()) {

        if (filterMetadataDTO.getProperty().equals("search")) {
          hqlBuilder.append(" and lower(s.text) like '%" + filterMetadataDTO.getValue().toLowerCase() + "%' ");
          countHqlBuilder.append(" and lower(s.text) like '%" + filterMetadataDTO.getValue().toLowerCase() + "%' ");
          continue;
        }

        if (filterMetadataDTO.getProperty().equals("category")) {

          switch (filterMetadataDTO.getValue()) {
            case "DAY":
              hqlBuilder.append(" and s.date > to_timestamp('" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + "', 'dd/MM/yyyy') ");
              countHqlBuilder.append(" and s.date > to_timestamp('" + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + "', 'dd/MM/yyyy') ");
              break;
            case "MONTH":
              Calendar date = Calendar.getInstance();
              date.set(Calendar.DAY_OF_MONTH, 1);
              hqlBuilder.append(" and s.date > to_timestamp('" + new SimpleDateFormat("dd/MM/yyyy").format(date.getTime()) + "', 'dd/MM/yyyy') ");
              countHqlBuilder.append(
                " and s.date > to_timestamp('" + new SimpleDateFormat("dd/MM/yyyy").format(date.getTime()) + "', 'dd/MM/yyyy') ");
              break;
            case "POPULAR":
              break;
            default:
              break;
          }
          continue;
        }

        hqlBuilder.append(" and s." + filterMetadataDTO.getProperty() + " = " + filterMetadataDTO.getValue() + " ");
        countHqlBuilder.append(" and s." + filterMetadataDTO.getProperty() + " = " + filterMetadataDTO.getValue() + " ");
      }
    }

  }

  public static void orderBy(PageDTO dataTableDTO, StringBuilder hqlBuilder, String defaultProperty) {

    if (dataTableDTO.getSortField() != null && !dataTableDTO.getSortField().equalsIgnoreCase("null")) {
      String order = dataTableDTO.getSortOrder() == 1 ? " asc" : " desc";
      hqlBuilder.append(" order by ").append(" " + dataTableDTO.getSortField()).append(order);
    }
    else {
      hqlBuilder.append(" order by " + defaultProperty + " desc");
    }

  }

}

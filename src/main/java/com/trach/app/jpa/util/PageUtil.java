package com.trach.app.jpa.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by ToZla on 13/10/2016.
 */
public class PageUtil {

    public static <T> Page<T> getPage(Query query, Query countQuery, Integer firstResult, Integer maxResult,
                    Class<T> clazz) {

        Long total = (Long)countQuery.getSingleResult();

        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);

        List<T> content = query.getResultList();

        Pageable pageable = new PageRequest(firstResult / maxResult, maxResult);

        return new PageImpl<T>(content, pageable, total);
    }

}

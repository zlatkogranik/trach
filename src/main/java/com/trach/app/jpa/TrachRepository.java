package com.trach.app.jpa;

import com.trach.app.jpa.custom.TrachRepositoryCustom;
import com.trach.app.model.Trach;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface TrachRepository extends Repository<Trach, Long>, TrachRepositoryCustom {

  public Trach findTrachById(Integer id);

  public Trach findTrachByUuid(String uuid);

  @Query("select t from Trach t where lower(t.text) like lower(?1) ")
  public Trach findTrachByText(String text);

  @Query("select t from Trach t left join t.tags tg where tg.id = ?1")
  public Trach findTrachByTag(String text);

  @Modifying
  @Query("update Trach t set t.likes = t.likes + 1 where t.id = ?1")
  public void increaseLike(Integer id);

  @Modifying
  @Query("update Trach t set t.unlikes = t.unlikes + 1 where t.id = ?1")
  public void increaseUnlike(Integer id);

  @Modifying
  @Query("update Trach t set t.likes = t.likes - 1 where t.id = ?1")
  public void decreaseLike(Integer id);

  @Modifying
  @Query("update Trach t set t.unlikes = t.unlikes - 1 where t.id = ?1")
  public void decreaseUnlike(Integer id);

  @Query("select count(t) from Trach t where t.approved = true")
  public Integer countApprovedTraches();

  @Modifying
  @Query("delete from Trach t where t.id = ?1")
  public Integer deleteTrachById(Integer id);

  @Modifying
  @Query("update Trach t set t.shares = t.shares + 1 where t.id = ?1")
  public void increaseShare(Integer id);

  @Modifying
  @Query("update Trach t set t.comment = t.comment + 1 where t.id = ?1")
  public void increaseComment(Integer id);

  @Query("select sum(t.shares) from Trach t ")
  public Integer countShares();

}

package com.trach.app.jpa;

import com.trach.app.jpa.custom.TagRepositoryCustom;
import com.trach.app.model.Tag;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface TagRepository extends Repository<Tag, Long>, TagRepositoryCustom {

  public Tag findTagById(Integer id);

  @Query("select t from Tag t where lower(t.text) = lower(?1)")
  public Tag findTagByText(String text);

  @Modifying
  @Query("update Tag t set t.count = t.count + 1 where lower(t.text) = lower(?1)")
  public void increaseCount(String text);

}

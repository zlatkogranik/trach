/**
 * Created by ToZla on 12/10/2016.
 */
export class LocalStorageService {

  public static get(key: string): string {
    return localStorage.getItem(key);
  }

  public static put(key: string, value: string): void {
    localStorage.setItem(key, value);
  }

  public static getJSON(key: string): Object {
    var value = localStorage.getItem(key);
    return JSON.parse(value);
  }

  public static putJSON(key: string, value: Object): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  public static hasOwnProperty(property: string): boolean {
    return LocalStorageService.hasOwnProperty(property);
  }

}

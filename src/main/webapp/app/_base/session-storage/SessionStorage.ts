/**
 * Created by ToZla on 07/11/2016.
 */
export class SessionStorageService {

    public static get(key: string): string {
        return sessionStorage.getItem(key);
    }

    public static put(key: string, value: string): void {
        sessionStorage.setItem(key, value);
    }

    public static getJSON(key: string): string {
        var value = sessionStorage.getItem(key);
        return JSON.parse(value);
    }

    public static putJSON(key: string, value: Object): void {
        sessionStorage.setItem(key, JSON.stringify(value));
    }

}
import {NgModule, Injector} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {XHRBackend, RequestOptions, HttpModule} from "@angular/http";
import "./rxjs-extensions";
import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./app-routing.module";
import {HomePageComponent} from "./home/home-page.component";
import {TrachService} from "./trach/trach.service";
import {TrachDetailsComponent} from "./trach/trach-details.component";
import {TrachCreateComponent} from "./trach/trach-create.component";
import {HeaderComponent} from "./header/header.component";
import {FooterComponent} from "./footer/footer.component";
import {HttpService, provideHttpService} from "./_shared/common/http/HttpService";
import {AlertService} from "./_shared/common/alert/AlertService";
import {ToasterModule} from "./_shared/common/toaster/toaster.module";
import {TagService} from "./tag/tag.service";
import {TrachSearchListComponent} from "./trach/trach-search-list.component";
import {TrachListComponent} from "./trach/trach-list.component";
import {TrachStatisticsComponent} from "./trach/trach-statistics.component";
import {TimeAgoPipe} from "./_shared/common/pipes/time-ago.pipe";
import {CommentService} from "./comment/comment.service";
import {StatisticsService} from "./statistics/statistics.service";
import {InfiniteScroll} from "./_shared/common/infinite-scroll/infinite-scroll";
import {AdminModule} from "./admin/admin.module";
import {AuthGuard} from "./_guards/auth.guard";
import {TranslateModule} from "./translate/translate.module";
import {TrachShareComponent} from "./trach/trach-share.component";
import {FacebookService} from "./_shared/common/fb/facebook-sdk";
import {AdminTrachDetailsComponent} from "./admin/trach/admin-trach-details.component";
import {AdminCommentDetailsComponent} from "./admin/comment/admin-comment-details.component";
import {TermsComponent} from "./terms/terms.component";
import {AboutComponent} from "./about/about.component";
import {MarketingComponent} from "./marketing/marketing.component";
import {FAQComponent} from "./faq/faq.component";
import {ContactComponent} from "./contact/contact.component";
import {LocalStorageComponent} from "./localstorage/local-storage.component";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ToasterModule.forRoot(),
    TranslateModule,
    AdminModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomePageComponent,
    TrachDetailsComponent,
    TrachCreateComponent,
    TrachSearchListComponent,
    TrachListComponent,
    TrachStatisticsComponent,
    TrachShareComponent,
    AdminTrachDetailsComponent,
    AdminCommentDetailsComponent,
    TermsComponent,
    AboutComponent,
    MarketingComponent,
    FAQComponent,
    ContactComponent,
    LocalStorageComponent,
    TimeAgoPipe,
    InfiniteScroll
  ],
  providers: [
    TrachService,
    AlertService,
    TagService,
    CommentService,
    StatisticsService,
    FacebookService,
    AuthGuard,
    {provide: HttpService, useFactory: provideHttpService, deps: [XHRBackend, RequestOptions, AlertService, Injector]}
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}

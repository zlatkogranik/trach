import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {HomePageComponent} from "./home/home-page.component";
import {TrachSearchListComponent} from "./trach/trach-search-list.component";
import {AdminHomeComponent} from "./admin/admin-home.component";
import {AuthGuard} from "./_guards/auth.guard";
import {AdminTrachDetailsComponent} from "./admin/trach/admin-trach-details.component";
import {AdminCommentDetailsComponent} from "./admin/comment/admin-comment-details.component";
import {TermsComponent} from "./terms/terms.component";
import {AboutComponent} from "./about/about.component";
import {ContactComponent} from "./contact/contact.component";
import {FAQComponent} from "./faq/faq.component";
import {MarketingComponent} from "./marketing/marketing.component";
import {LocalStorageComponent} from "./localstorage/local-storage.component";

const routes: Routes = [

  {path: '', redirectTo: '/home', pathMatch: 'full'},

  {path: 'home', component: HomePageComponent},
  {path: 'home/:sort', component: HomePageComponent},

  {path: 'tag/:tag', component: TrachSearchListComponent},
  {path: 'tag/:tag/:sort', component: TrachSearchListComponent},
  {path: 'type/:type', component: TrachSearchListComponent},
  {path: 'type/:type/:sort', component: TrachSearchListComponent},
  {path: 'trach/:id', component: TrachSearchListComponent},
  {path: 'search/:search', component: TrachSearchListComponent},

  {path: 'admin/home', component: AdminHomeComponent, canActivate: [AuthGuard]},
  {path: 'admin/trach/:id', component: AdminTrachDetailsComponent, canActivate: [AuthGuard]},
  {path: 'admin/comment/:id', component: AdminCommentDetailsComponent, canActivate: [AuthGuard]},

  {path: 'about', component: AboutComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'faq', component: FAQComponent},
  {path: 'marketing', component: MarketingComponent},
  {path: 'terms', component: TermsComponent},

  {path: 'ls/:key/:value', component: LocalStorageComponent},

  {path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

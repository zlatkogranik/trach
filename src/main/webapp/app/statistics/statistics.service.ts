/**
 * Created by ToZla on 16/02/2017.
 */
import {Injectable} from "@angular/core";
import {HttpService} from "../_shared/common/http/HttpService";
import {Observable} from "rxjs";
import {Statistics} from "./statistics";

@Injectable()
export class StatisticsService {

  private url: string = "api/statistics";

  constructor(private http: HttpService) {

  }

  getStatistics(): Observable<Statistics> {
    return this.http.get(this.url)
      .map(response => <Statistics>response.json());
  }
}

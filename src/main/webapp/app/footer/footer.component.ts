/**
 * Created by ToZla on 11/02/2017.
 */
import {Component, OnInit} from "@angular/core";

@Component({
  moduleId: module.id,
  selector: 'trach-footer',
  templateUrl: 'footer.component.html',
  styleUrls: ['footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}

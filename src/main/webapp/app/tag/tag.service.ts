/**
 * Created by ToZla on 16/02/2017.
 */
import {Injectable} from "@angular/core";
import {HttpService} from "../_shared/common/http/HttpService";
import {Observable} from "rxjs";
import {Tag} from "./tag";

@Injectable()
export class TagService {

  private url: string = "api/tag";


  constructor(private http: HttpService) {

  }

  getTagById(id: number): Observable<Tag[]> {
    return this.http.get(this.url + "/" + id)
      .map(response => <Tag[]>response.json());
  }

  getTagByText(tag: string): Observable<Tag> {
    return this.http.post(this.url + "/text", tag)
      .map(response => <Tag>response.json());
  }

}

/**
 * Created by ToZla on 09/11/2016.
 */

import {NgModule, ModuleWithProviders} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ToasterComponent} from "./toaster.component";
import {ToasterConfig, ToasterService} from "./toaster.service";
import {ToastComponent} from "./toast.component";

@NgModule({
    imports: [CommonModule],
    declarations: [ToastComponent, ToasterComponent],
    exports: [ToastComponent, ToasterComponent]
})
export class ToasterModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ToasterModule,
            providers: [ToasterConfig, ToasterService]
        };
    }
}
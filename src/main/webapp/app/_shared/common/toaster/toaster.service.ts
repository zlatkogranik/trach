import {Injectable, EventEmitter} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {isNumber, isString, isFunction} from "../../util/utils";

/**
 * Options to configure specific Toast
 */
export interface ToastOptions {
  title: string;
  msg?: string;
  theme?: string;
  timeout?: number;
  onAdd?: Function;
  onRemove?: Function;
}

/**
 * Structure of Toast
 */
export interface ToastData {
  id: number;
  title: string;
  msg: string;
  type: string;
  icon: string;
  timeout: number;
  onAdd: Function;
  onRemove: Function;
  onClick: Function;
}

/**
 * Default configuration foa all toasts and toaster container
 */
@Injectable()
export class ToasterConfig {

  // Maximum number of toasties to show at once
  limit: number = 5;

  // The window position where the toast pops up. Possible values
  // bottom-right, bottom-left, top-right, top-left, top-center, bottom-center, center-center
  position: string = 'top-center';

  // How long (in miliseconds) the toast shows before it's removed. Set to null/0 to turn off.
  timeout: number = 5000;

}


/**
 * Toasty service helps create different kinds of Toasts
 */
@Injectable()
export class ToasterService {

  uniqueCounter: number = 0;

  private toastsEmitter: EventEmitter<ToastData> = new EventEmitter<ToastData>();

  private clearEmitter: EventEmitter<number> = new EventEmitter<number>();

  constructor(private config: ToasterConfig) {
  }

  getToasts(): Observable<ToastData> {
    return this.toastsEmitter.asObservable();
  }

  getClear(): Observable<number> {
    return this.clearEmitter.asObservable();
  }

  default(options: ToastOptions|string|number): void {
    this.add(options, 'default');
  }

  info(options: ToastOptions|string|number): void {
    this.add(options, 'info');
  }

  success(options: ToastOptions|string|number): void {
    this.add(options, 'success');
  }

  wait(options: ToastOptions|string|number): void {
    this.add(options, 'wait');
  }

  error(options: ToastOptions|string|number): void {
    this.add(options, 'danger');
  }

  warning(options: ToastOptions|string|number): void {
    this.add(options, 'warning');
  }

  private add(options: ToastOptions|string|number, type: string) {

    let toastOptions: ToastOptions;

    if (isString(options) && options !== '' || isNumber(options)) {
      toastOptions = <ToastOptions>{
        title: options.toString()
      };
    } else {
      toastOptions = <ToastOptions>options;
    }

    if (!toastOptions || !toastOptions.title && !toastOptions.msg) {
      throw new Error('toaster: No toast title or message specified!');
    }

    type = type || 'default';

    // Set a unique counter for an id
    this.uniqueCounter++;

    let icon = 'default';

    switch (type) {
      case 'info':
        icon = 'fa-info-circle';
        break;
      case 'danger':
        icon = 'fa-minus-circle';
        break;
      case 'success':
        icon = 'fa-check-circle';
        break;
      case 'warning':
        icon = 'fa-warning';
        break;
    }


    let toast: ToastData = <ToastData>{
      id: this.uniqueCounter,
      title: toastOptions.title,
      msg: toastOptions.msg,
      type: 'alert-' + type,
      icon: icon,
      onAdd: toastOptions.onAdd && isFunction(toastOptions.onAdd) ? toastOptions.onAdd : null,
      onRemove: toastOptions.onRemove && isFunction(toastOptions.onRemove) ? toastOptions.onRemove : null
    };

    // If there's a timeout individually or globally, set the toast to timeout
    // Allows a caller to pass null/0 and override the default. Can also set the default to null/0 to turn off.
    toast.timeout = toastOptions.hasOwnProperty('timeout') ? toastOptions.timeout : this.config.timeout;

    // Push up a new toast item
    this.toastsEmitter.next(toast);
    // If we have a onAdd function, call it here
    if (toastOptions.onAdd && isFunction(toastOptions.onAdd)) {
      toastOptions.onAdd.call(this, toast);
    }
  }

  clearAll() {
    this.clearEmitter.next(null);
  }

  clear(id: number) {
    this.clearEmitter.next(id);
  }

  // Checks whether the local option is set, if not, checks the global config
  private _checkConfigItem(config: any, options: any, property: string) {
    if (options[property] === false) {
      return false;
    } else if (!options[property]) {
      return config[property];
    } else {
      return true;
    }
  }
}

/**
 * Created by ToZla on 09/11/2016.
 */

import {Component, Input, Output, EventEmitter} from '@angular/core';

import {ToastData} from './toaster.service';

@Component({
    moduleId: module.id,
    selector: 'toast',
    templateUrl: 'toast.component.html',
    styleUrls: ['toast.component.css']
})
export class ToastComponent {

    @Input() toast: ToastData;
    @Output('closeToast') closeToastEvent = new EventEmitter();

    close($event: any) {
        $event.preventDefault();
        this.closeToastEvent.next(this.toast);
    }
}

/**
 * Created by ToZla on 09/11/2016.
 */
import {Component, OnInit} from "@angular/core";
import {ToasterService, ToastData, ToasterConfig} from "./toaster.service";
import {isFunction} from "../../util/utils";

/**
 * Toasty is container for Toast components
 */
@Component({
  moduleId: module.id,
  selector: 'toaster',
  templateUrl: 'toaster.component.html',
  styleUrls: ['toaster.component.css']
})
export class ToasterComponent implements OnInit {

  position: string = 'top-center';

  toasts: Array<ToastData> = [];

  constructor(private config: ToasterConfig, private toasterService: ToasterService) {
    this.position = config.position;
  }

  ngOnInit(): any {

    // We listen our service to recieve new toasts from it
    this.toasterService.getToasts().subscribe((toast: ToastData) => {

      // If we've gone over our limit, remove the earliest one from the array
      if (this.toasts.length >= this.config.limit) {
        this.toasts.shift();
      }

      this.toasts.push(toast);

      // If there's a timeout individually or globally, set the toast to timeout
      if (toast.timeout) {
        this._setTimeout(toast);
      }
    });

    // We listen clear all comes from service here.
    this.toasterService.getClear().subscribe((id: number) => {
      if (id) {
        this.clear(id);
      }
      this.clearAll();
    });
  }

  closeToast(toast: ToastData) {
    this.clear(toast.id);
  }

  clear(id: number) {
    if (id) {
      this.toasts.forEach((value: any, key: number) => {
        if (value.id === id) {
          if (value.onRemove && isFunction(value.onRemove)) {
            value.onRemove.call(this, value);
          }
          this.toasts.splice(key, 1);
        }
      });
    } else {
      throw new Error('Please provide id of Toast to close');
    }
  }

  clearAll() {
    this.toasts.forEach((value: any, key: number) => {
      if (value.onRemove && isFunction(value.onRemove)) {
        value.onRemove.call(this, value);
      }
    });
    this.toasts = [];
  }

  private _setTimeout(toast: ToastData) {
    window.setTimeout(() => {
      this.clear(toast.id);
    }, toast.timeout);
  }
}

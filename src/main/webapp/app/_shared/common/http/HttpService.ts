import "rxjs/Rx";
import {Injectable, Injector} from "@angular/core";
import {Http, ConnectionBackend, RequestOptions, RequestOptionsArgs, Response, Headers, Request, XHRBackend} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {AlertService} from "../alert/AlertService";
import {LocalStorageService} from "../../../_base/local-storage/LocalStorageService";

@Injectable()
export class HttpService extends Http {

  constructor(backend: ConnectionBackend,
              defaultOptions: RequestOptions,
              private alertService: AlertService) {
    super(backend, defaultOptions);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return super.request(url, options);
  }

  getLocal(url: string, options?: RequestOptionsArgs): Observable<any> {
    return super.get(url, options);
  }

  get(url: string, options?: RequestOptionsArgs): Observable<any> {
    this.requestInterceptor();
    return super.get(url, this.requestOptions(options))
      .catch(this.onCatch)
      .do((res: Response) => {
        this.onSubscribeSuccess(res);
      }, (error: any) => {
        this.onSubscribeError(error);
      })
      .finally(() => {
        this.onFinally();
      });
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    this.requestInterceptor();
    return super.post(url, body, this.requestOptions(options))
      .catch(this.onCatch)
      .do((res: Response) => {
        this.onSubscribeSuccess(res);
      }, (error: any) => {
        this.onSubscribeError(error);
      })
      .finally(() => {
        this.onFinally();
      });
  }

  put(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    this.requestInterceptor();
    return super.put(url, body, this.requestOptions(options))
      .catch(this.onCatch)
      .do((res: Response) => {
        this.onSubscribeSuccess(res);
      }, (error: any) => {
        this.onSubscribeError(error);
      })
      .finally(() => {
        this.onFinally();
      });
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<any> {
    this.requestInterceptor();
    return super.delete(url, this.requestOptions(options))
      .catch(this.onCatch)
      .do((res: Response) => {
        this.onSubscribeSuccess(res);
      }, (error: any) => {
        this.onSubscribeError(error);
      })
      .finally(() => {
        this.onFinally();
      });
  }


  private requestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
    }

    //options.headers.append('Authorization', 'Bearer ' + LocalStorageService.get('jwtToken'));
    options.headers.append('Content-Type', 'application/json');
    options.headers.append('x-tzl', 's3cR3t');
    if (localStorage.getItem('xc') != null) {
      options.headers.append('xs', localStorage.getItem('xc'));
    }

    if (localStorage.getItem('th-ut') != null) {
      options.headers.append('th-ut', localStorage.getItem('th-ut'));
    }

    return options;

  }

  public getAuthorisationHeader(): string {
    return 'Bearer ' + LocalStorageService.get('jwtToken');
  }


  private requestInterceptor(): void {

  }


  private responseInterceptor(): void {

  }

  private onCatch(error: any, caught: Observable<any>): Observable<any> {
    return Observable.throw(error);
  }

  private onSubscribeSuccess(res: Response): void {
    this.alertService.processResponse(res);

    let uuid = res.headers.get('th-ut');

    if (uuid != undefined) {
      LocalStorageService.put('th-ut', uuid);
    }

  }

  private onSubscribeError(error: any): void {
    this.alertService.processResponse(error);
  }

  private onFinally(): void {
    this.responseInterceptor();
  }

}

export function provideHttpService(backend: XHRBackend, requestOptions?: RequestOptions, alertService?: AlertService, injector?: Injector) {
  return new HttpService(backend, requestOptions, alertService);
}

/**
 * Created by ToZla on 10/01/2017.
 */

export class FileUpload {

    constructor(private relativeFilePath: string,
                private fileName: string,
                private fileSize: number,
                private contentType: string,
                private origFileName: string,
                private filePreview: string) {
    }

}
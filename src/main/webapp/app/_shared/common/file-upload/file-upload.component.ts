import {Component, OnInit, AfterContentInit, Input, Output, ContentChildren, QueryList, EventEmitter, TemplateRef} from "@angular/core";
import {ContentTemplate} from "../util/template-content";
import {DomSanitizer} from "@angular/platform-browser";
import {FileUpload} from "./file-upload";
import {HttpService} from "../http/HttpService";
import {AlertService} from "../alert/AlertService";

/**
 * Created by ToZla on 10/01/2017.
 */

@Component({
  moduleId: module.id,
  selector: 'wf-file-upload',
  templateUrl: 'file-upload.component.html',
  styleUrls: ['file-upload.component.css']
})
export class FileUploadComponent implements OnInit, AfterContentInit {

  @Input() url: string = "api_v2/file_upload";

  @Input() name: string;

  @Input() multiple: boolean = false;

  @Input() accept: string;

  @Input() disabled: boolean;

  @Input() style: string;

  @Input() styleClass: string;

  @Input() previewWidth: number = 50;

  @Input() types: string = "";

  @Input() maxFileSize: number = 0;

  @Input() maxImgWidth: number = 0;

  @Input() maxImgHeight: number = 0;

  @Output() onBeforeUpload: EventEmitter<any> = new EventEmitter();

  @Output() onUpload: EventEmitter<FileUpload[]> = new EventEmitter<FileUpload[]>();

  @Output() onError: EventEmitter<any> = new EventEmitter();

  @Output() onClear: EventEmitter<any> = new EventEmitter();

  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  @ContentChildren(ContentTemplate) templates: QueryList<any>;

  protected files: File[];

  protected progress: number = 0;

  public dragHighlight: boolean;

  protected fileTemplate: TemplateRef<any>;

  protected contentTemplate: TemplateRef<any>;

  constructor(private sanitizer: DomSanitizer,
              private httpService: HttpService,
              private alertService: AlertService) {

  }

  ngOnInit() {
    this.files = [];
  }

  ngAfterContentInit(): void {
    this.templates.forEach((item) => {
      switch (item.type) {
        case 'file':
          this.fileTemplate = item.template;
          break;
        case 'content':
          this.contentTemplate = item.template;
          break;
        default:
          this.fileTemplate = item.template;
          break;
      }
    });
  }

  onChooseClick(event: any, fileInput: any) {
    fileInput.value = null;
    fileInput.click();
  }

  onFileSelect(event: any) {

    let files = event.dataTransfer ? event.dataTransfer.files : event.target.files;

    for (let i = 0; i < files.length; i++) {

      let file = files[i];

      if (this.isImage(file)) {
        file.objectURL = this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(files[i])));
      }

      if (this.multiple) {
        this.files.push(files[i]);
      } else {
        this.files = [files[i]];
      }
    }

    this.onSelect.emit({originalEvent: event, files: files});

    if (this.files) {
      this.upload();
    }
  }

  isImage(file: File): boolean {
    return /^image\//.test(file.type);
  }

  onImageLoad(img: any) {
    window.URL.revokeObjectURL(img.src);
  }

  upload() {

    let xhr = new XMLHttpRequest();
    let formData = new FormData();

    formData.append("types", this.types);
    formData.append("size", this.maxFileSize);
    formData.append("imgWidth", this.maxImgWidth);
    formData.append("imgHeight", this.maxImgHeight);

    for (let i = 0; i < this.files.length; i++) {
      formData.append("files[]", this.files[i], this.files[i].name);
    }

    xhr.upload.addEventListener('progress', (e: ProgressEvent) => {
      if (e.lengthComputable) {
        this.progress = Math.round((e.loaded * 100) / e.total);
      }
    }, false);

    xhr.onreadystatechange = () => {

      if (xhr.readyState == 4) {

        this.progress = 0;

        if (xhr.status == 200) {
          this.onUpload.emit(JSON.parse(xhr.responseText));
        }
        else {
          this.onError.emit({xhr: xhr, files: this.files});
        }

        this.alertService.processXhrResponse(xhr);

      }
    };

    xhr.open('POST', this.url, true);

    this.onBeforeUpload.emit({
      'xhr': xhr,
      'formData': formData
    });

    xhr.setRequestHeader('Authorization', this.httpService.getAuthorisationHeader());

    xhr.send(formData);
  }

  clear() {
    this.files = [];
    this.onClear.emit();
  }

  remove(index: number) {
    this.files.splice(index, 1);
  }

  hasFiles(): boolean {
    return this.files && this.files.length > 0;
  }

  onDragEnter(e: any) {
    if (!this.disabled) {
      e.stopPropagation();
      e.preventDefault();
    }
  }

  onDragOver(e: any) {
    if (!this.disabled) {
      this.dragHighlight = true;
      e.stopPropagation();
      e.preventDefault();
    }
  }

  onDragLeave(e: any) {
    if (!this.disabled) {
      this.dragHighlight = false;
    }
  }

  onDrop(e: any) {
    if (!this.disabled) {
      this.dragHighlight = false;
      e.stopPropagation();
      e.preventDefault();

      this.onFileSelect(e);
    }
  }

  formatSize(bytes: any) {
    if (bytes == 0) {
      return '0 B';
    }
    let k = 1000,
      dm = 3,
      sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

}

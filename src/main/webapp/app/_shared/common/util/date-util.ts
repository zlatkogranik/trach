import {DatePipe} from "@angular/common";
/**
 * Created by ToZla on 09/12/2016.
 */
export class DateFormatter {

    public static format(date: Date, format?: string, locale?: string) {

        let datePipe = new DatePipe('en-US');

        if (locale != undefined) {
            datePipe = new DatePipe(locale);
        }

        return datePipe.transform(date, format);
    }

}
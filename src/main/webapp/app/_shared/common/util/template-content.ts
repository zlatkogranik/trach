/**
 * Created by ToZla on 09/01/2017.
 */
import {Input, TemplateRef, Directive} from "@angular/core";

@Directive({
    selector: '[wf-template]',
    host: {}
})
export class ContentTemplate {

    @Input() type: string;

    constructor(protected template: TemplateRef<any>) {

    }

}
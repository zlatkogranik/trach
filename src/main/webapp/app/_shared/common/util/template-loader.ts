/**
 * Created by ToZla on 09/01/2017.
 */
import {Component, Input, TemplateRef, ViewContainerRef} from "@angular/core";

@Component({
    selector: 'wf-template-loader',
    template: ``
})
export class TemplateLoader {

    @Input() template: TemplateRef<any>;

    constructor(protected viewContainer: ViewContainerRef) {
    }

    ngOnInit() {
        if (this.template) {
            this.viewContainer.createEmbeddedView(this.template, {});
        }
    }
}
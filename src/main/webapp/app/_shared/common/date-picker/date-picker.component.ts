/**
 * Created by ToZla on 09/12/2016.
 */
import {Component, Input, Output, EventEmitter, ElementRef, HostListener} from "@angular/core";

@Component({
    moduleId: module.id,
    selector: 'wf-date-picker',
    templateUrl: 'date-picker.component.html',
    styleUrls: ['date-picker.component.css']
})
export class DatePickerComponent {

    public dt: Date = new Date();

    @Input() minDate: Date = void 0;
    @Input() showWeeks: boolean = false;
    @Input() popupPicker: boolean = false;

    @Output() onSelect: EventEmitter<any> = new EventEmitter();

    private initialization = true;

    displayPicker: boolean = false;

    constructor(private _eref: ElementRef){

    }

    onChange() {
        if (this.initialization) {
            this.initialization = false;
        } else {
            this.onSelect.emit(this.dt);
        }
    }

    showPickerBtnClicked(){
        this.initialization = true;
        this.displayPicker = true;
    }

    @HostListener('document:click', ['$event'])
    onDocumentClick(event: any){
        if (!this._eref.nativeElement.contains(event.target)){
            this.displayPicker = false;
        }
    }
}
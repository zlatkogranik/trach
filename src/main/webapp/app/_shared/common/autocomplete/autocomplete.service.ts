import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Response} from "@angular/http";
import {HttpService} from "../http/HttpService";

@Injectable()
export class AutocompleteService {

  private autocompleteUrl = 'api_v2/autocomplete';

  constructor(private http: HttpService) {
  }

  getData(type: String, query: String): Observable<any[]> {
    return this.http.post(this.autocompleteUrl, {type: type, query: query})
      .map((response: Response) => {
        return <any[]> response.json();
      });
  }
}

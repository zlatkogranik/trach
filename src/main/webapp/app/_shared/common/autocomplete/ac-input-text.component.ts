import {Directive,ElementRef,HostListener} from '@angular/core';

@Directive({
    selector: '[pInputText]',
    host: {
        '[class.ui-inputtext]': 'true',
        '[class.ui-corner-all]': 'true',
        '[class.ui-state-default]': 'true',
        '[class.ui-widget]': 'true',
        '[class.ui-state-hover]': 'hover',
        '[class.ui-state-focus]': 'focus',
        '[class.ui-state-disabled]': 'disabled',
        '[class.ui-state-filled]': 'filled'
    }
})
export class AcInputText {

    hover: boolean;

    focus: boolean;

    constructor(public el: ElementRef) {}

    @HostListener('mouseover', ['$event'])
    onMouseover(e: any) {
        this.hover = true;
    }

    @HostListener('mouseout', ['$event'])
    onMouseout(e: any) {
        this.hover = false;
    }

    @HostListener('focus', ['$event'])
    onFocus(e: any) {
        this.focus = true;
    }

    @HostListener('blur', ['$event'])
    onBlur(e: any) {
        this.focus = false;
    }

    get disabled(): boolean {
        return this.el.nativeElement.disabled;
    }

    get filled(): boolean {
        return this.el.nativeElement.value&&this.el.nativeElement.value.length;
    }
}
/**
 * Created by ToZla on 27/10/2016.
 */
import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {ToasterService, ToastOptions} from "../toaster/toaster.service";

@Injectable()
export class AlertService {

  static INFO_MESSAGE_HEADER: string = "infoMessage";
  static ERROR_MESSAGE_HEADER: string = "errorMessage";
  static SUCCESS_MESSAGE_HEADER: string = "successMessage";
  static WARNING_MESSAGE_HEADER: string = "warningMessage";

  constructor(private toasterService: ToasterService) {

  }

  info(message: string, title?: string, onAdd?: any, onRemove?: any) {
    var toast = this.prepateToastOptions(message, title, onAdd, onRemove);
    this.toasterService.info(toast);
  }

  error(message: string, title?: string, onAdd?: any, onRemove?: any) {
    var toast = this.prepateToastOptions(message, title, onAdd, onRemove);
    this.toasterService.error(toast);
  }

  success(message: string, title?: string, onAdd?: any, onRemove?: any) {
    var toast = this.prepateToastOptions(message, title, onAdd, onRemove);
    this.toasterService.success(toast);
  }

  warning(message: string, title?: string, onAdd?: any, onRemove?: any) {
    var toast = this.prepateToastOptions(message, title, onAdd, onRemove);
    this.toasterService.warning(toast);
  }

  prepateToastOptions(message: string, title?: string, onAdd?: any, onRemove?: any): ToastOptions {
    var toastOptions: ToastOptions = {
      title: message
    }
    if (title) {
      toastOptions.msg = title;
    }
    if (onAdd) {
      toastOptions.onAdd = onAdd;
    }
    if (onRemove) {
      toastOptions.onRemove = onRemove;
    }
    return toastOptions;
  }

  processResponse(response: Response) {

    if (response.headers.get(AlertService.INFO_MESSAGE_HEADER)) {
      let message = response.headers.get(AlertService.INFO_MESSAGE_HEADER);
      this.info(message);
    }

    if (response.headers.get(AlertService.ERROR_MESSAGE_HEADER)) {
      let message = response.headers.get(AlertService.ERROR_MESSAGE_HEADER);
      this.error(message);
    }

    if (response.headers.get(AlertService.SUCCESS_MESSAGE_HEADER)) {
      let message = response.headers.get(AlertService.SUCCESS_MESSAGE_HEADER);
      this.success(message);
    }

    if (response.headers.get(AlertService.WARNING_MESSAGE_HEADER)) {
      let message = response.headers.get(AlertService.WARNING_MESSAGE_HEADER);
      this.warning(message);
    }

  }

  processXhrResponse(xhr: XMLHttpRequest) {

    if (xhr.getResponseHeader(AlertService.INFO_MESSAGE_HEADER)) {
      let message = xhr.getResponseHeader(AlertService.INFO_MESSAGE_HEADER);
      this.info(message);
    }

    if (xhr.getResponseHeader(AlertService.ERROR_MESSAGE_HEADER)) {
      let message = xhr.getResponseHeader(AlertService.ERROR_MESSAGE_HEADER);
      this.error(message);
    }

    if (xhr.getResponseHeader(AlertService.SUCCESS_MESSAGE_HEADER)) {
      let message = xhr.getResponseHeader(AlertService.SUCCESS_MESSAGE_HEADER);
      this.success(message);
    }

    if (xhr.getResponseHeader(AlertService.WARNING_MESSAGE_HEADER)) {
      let message = xhr.getResponseHeader(AlertService.WARNING_MESSAGE_HEADER);
      this.warning(message);
    }

  }
}

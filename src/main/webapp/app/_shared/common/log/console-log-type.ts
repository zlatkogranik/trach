/**
 * Created by ToZla on 09/12/2016.
 */
export enum ConsoleLogLevel{
    TRACE,
    DEBUG,
    ERROR
}
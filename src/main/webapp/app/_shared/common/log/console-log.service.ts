import {ConsoleLogLevel} from "./console-log-type";
import {LocalStorageService} from "../../../_base/local-storage/LocalStorageService";
import {LOG_LEVEL} from "../../../_base/local-storage/LocalStorageAttributes";
/**
 * Created by ToZla on 09/12/2016.
 */
export class ConsoleLog {

    static trace(message: string) {
        if (ConsoleLog.getLevel() == ConsoleLogLevel.TRACE) {
            console.trace(message);
        }
    }

    static debug(message: string) {
        let level = ConsoleLog.getLevel();
        if (level == ConsoleLogLevel.TRACE || level == ConsoleLogLevel.DEBUG) {
            console.debug(message);
        }
    }

    static error(message: string) {
        console.error(message);
    }

    static getLevel(): ConsoleLogLevel {

        let level = LocalStorageService.get(LOG_LEVEL);

        if (level != undefined) {
            switch (level.toUpperCase()) {
                case "ERROR":
                    return ConsoleLogLevel.ERROR;
                case "TRACE":
                    return ConsoleLogLevel.TRACE;
                case "DEBUG":
                    return ConsoleLogLevel.DEBUG;
            }
        }

        return ConsoleLogLevel.ERROR;
    }

}

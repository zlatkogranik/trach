import {Directive, ElementRef, Input, Output, EventEmitter} from "@angular/core";
import {NgModel} from "@angular/forms";

@Directive({
  selector: '[InfiniteScroll]',
  providers: [NgModel],
  host: {
    '(scroll)':'onScroll($event)'
  }
})
export class InfiniteScroll {

  public _element:any;
  public _count:number;

  @Input() scrollDistance: number = 3;
  @Output() onScrollMethod = new EventEmitter<any>();

  constructor(public element:ElementRef) {
    this._element = this.element.nativeElement;
    if(!this.scrollDistance) {
      this.scrollDistance = 1;
    }
  }

  onScroll($event: any) {
    this._count++;
    if(this._element.scrollTop + this._element.clientHeight >= this._element.scrollHeight) {
      this.onScrollMethod.emit(null);
    }else {
      if(this._count % this.scrollDistance === 0) {
        this.onScrollMethod.emit(null);
      }
    }
  }
}

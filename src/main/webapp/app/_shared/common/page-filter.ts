import {PageFilterMetadata} from "./page-filter-metadata";
/**
 * Created by ToZla on 13/10/2016.
 */
export class PageFilter {

  constructor(public firstResult?: number,
              public maxResult?: number,
              public tags?: number[],
              public sortField?: string,
              public sortOrder?: number,
              public filterMetadata?: PageFilterMetadata[]) {
  }

}

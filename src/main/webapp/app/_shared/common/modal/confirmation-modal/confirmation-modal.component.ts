/**
 * Created by ToZla on 16/11/2016.
 */
import {Component, Output, EventEmitter} from "@angular/core";
// import {ModalDirective} from "ng2-bootstrap";

@Component({
  moduleId: module.id,
  selector: 'wf-confirmation-modal',
  templateUrl: 'confirmation-modal.component.html'
})
export class ConfirmationModalComponent {

  // @ViewChild("confirmationModal") public confirmationModal: ModalDirective;

  @Output() onYes: EventEmitter<any> = new EventEmitter();
  @Output() onNo: EventEmitter<any> = new EventEmitter();

  title: string;
  message: string;
  public action: string;
  private arg: any;

  constructor() {

  }

  showDialog(title: string, message: string, action?: string, arg?: any) {
    this.title = title;
    this.message = message;
    this.action = action;
    this.arg = arg;
    // this.confirmationModal.show();
  }

  showDeleteDialog(name: string, action?: string, arg?: any) {

    var titleKey = 'ng2.modal.dialog.delete.title';
    var messageKey = 'ng2.modal.dialog.delete.message';

    // this.showDialog(translations[titleKey].value, translations[messageKey].value, action, arg);
  }

  // yes() {
  //     this.confirmationModal.hide();
  //     this.onYes.emit({confirmed: true, action: this.action, arg: this.arg});
  // }
  //
  // no() {
  //     this.confirmationModal.hide();
  //     this.onNo.emit({confirmed: false, action: this.action, arg: this.arg});
  // }

}

/**
 * Created by ToZla on 13/10/2016.
 */
export class PageFilterMetadata {

  constructor(public property?: string,
              public value?: string,
              public matchMode?: string) {
  }

}

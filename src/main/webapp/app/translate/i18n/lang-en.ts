/**
 * Created by ToZla on 19/02/2017.
 */
export const LANG_EN_TRANS = {

  'title': 'TrachHub',
  'button.trach': 'T r a c h !',

  'about.h1': 'About Us',
  'about.goal': 'Goal',
  'about.goal.info': 'We\'re tired of gossip about celebrities, so we want to enable us to gossip about all of us, our everyday. Why are their gossips interesting than ours. Of course, taking into account everyone\'s privacy and personal data. Share gossip from your everyday life, taking care that no one can identify exactly who\'s the person (company, city) you are writing about, so it should not feel injured.',
  'about.vision': 'Vision',
  'about.vision.info': 'TrachHub.com The idea is to have fun and brought joy and love into the lives of all of us.',
  'about.regards': 'Kind Regards, Team TrachHub.com',

  'contact.h1': 'Contact',
  'contact.write': 'Write To Us!',
  'contact.write.info1': 'If you have any objection, advice, suggestion or compliment, please feel free to contact us at mail ',
  'contact.write.info2': '. We are willing to listen to the opinion of our users.',
  'contact.write.info3': 'Please before sending any questions, read',
  'contact.write.info4': 'and if you do not find the answer, write to us via email.',
  'contact.write.note': '*Note:',
  'contact.write.thanks': 'Thanks!',

  'faq.h1': 'FAQ',
  'faq.question1': '1. "Why do not you publish all Traches?"',
  'faq.question1.answer': 'Publication of all confessions is not possible for many reasons. Every day arrives incredible amount of your Traches, and it is necessary to make a selection. Some of them do not meet the basic rules of the site and all other unpublished Traches are not good enough voted by our Administration Team.',
  'faq.question2': '2. "Why my Trach is not published, even though it fulfills all rules?"',
  'faq.question2.answer': 'If the Trach fulfills all the criteria specified in the rules, it means that it has entered into aproval section, but was not voted by our Administration team.',
  'faq.question3': '3. "Why do not you approve all comments, although not containing profanity?"',
  'faq.question3.answer': 'Since everything is anonymous, anyone can write an inappropriate content or impairs the quality of content on the site, a variety of spam messages, etc... We strive to approve all comments. However, there are some comments aimed not have well-intentioned criticism or advice directed to the author, but express hate speech and sharp criticism, without constructive suggestions for improvement. Also, comments containing explicit vulgarity (uncensored) are not welcome on the site. If your comment has not been released, but feel that this is an error, send an email to trachhub@gmail.com and we will publish it.',

  'marketing.h1': 'Marketing',
  'marketing.h3': 'Advertising on TrachHub.com',
  'marketing.info': 'For all information regarding advertising or similar activities, please contact us at',

  'terms.h1': 'Terms of use',
  'terms.introduction': 'Introduction',
  'terms.introduction.info': 'TrachHub.com represents a collection of various human opinions, rumors, colloquially speaking celebrities, which many want to share with others or are afraid of the reaction environment. TrachHub.com is intended for all groups of people and individuals and is designed for every age. Using any part TrachHub.com portal and all its parts, you automatically accept all current copyright rules. Users are required to read the rules of use, and it is considered that the visitors of the TrachHub.com portal, or any portion thereof at any time to meet with the current rules and that they understand them completely. No part TrachHub.com portal can be used for illegal purposes, or for their promotion.',
  'terms.portal': 'TrachHub.com Portal',
  'terms.portal.info': 'TrachHub.com enables visitors in good faith and in the only purpose of fun to use the contents TrachHub.com portal. All visitors have the right to use the content TrachHub.com portal, they do not violate copyright rules. TrachHub.com portal consists of its own content, content partners and advertisers, free content, content created by visitors and links to external sites. TrachHub.com publishes content with good intent. All contents TrachHub.com portal at your own risk and TrachHub.com can not be held liable for any damages resulting from the use. Access TrachHub.com website and all its contents is allowed to persons of any age.',
  'terms.copyright': 'Copyright',
  'terms.copyright.info': 'TrachHub.com holds copyright on all authorized material (textual, visual and audio materials, databases and programming code). Unauthorized use of any portion of the portal is considered a violation of copyright TrachHub.com portals and subject to prosecution. Any commercial use or distribution without permission of the copyright owner (TrachHub, partners, users ...) is considered copyright infringement and liable to legal action. If you believe that TrachHub.com violated your copyright, the case will be considered instantly, and the problematic content will be removed immediately after the eventual establishment factual.',
  'terms.links': 'External Links',
  'terms.links.info': 'TrachHub.com contains links to sites outside its own portals. TrachHub.com links with good intent and can not be held responsible for the content outside the portal.',
  'terms.privacy': 'User Privacy',
  'terms.privacy.info': 'TrachHub.com may, in accordance with the law, gather certain data about its users received while using the portal (exclusively data about the computer, location and information on the Internet service provider). These data TrachHub.com used to have information on users who use it, and thus improve the portal and its contents additionally intended and adapted for the public which visits. TrachHub.com is committed to protecting the privacy of portal users, except in the case of serious violations TrachHub.com portal or illegal activities.',
  'terms.rules': 'Modifying the rules',
  'terms.rules.info': 'TrachHub reserves the right without prior notice to change or modify these rules. Using any content on Trachhub.com portal is considered that you are familiar with the latest rules.'

};

/**
 * Created by ToZla on 19/02/2017.
 */
export const LANG_RS_TRANS = {
  'title': 'TrachHub',
  'button.trach': 'T r a c h !',

  'about.h1' : 'O Nama',
  'about.goal' : 'Cilj',
  'about.goal.info' : 'Cilj nam je da promovišemo zdrav humor, tradicionalne vrednosti, ljubav i moralne vrednosti o kojima čini se, niko više ne vodi računa. Mi želimo da svakodnevno pokazujemo i dokazujemo da postoje divne ljudske priče, iskrena i emotivna priznanja koja čekaju negde skrivena i zaključana duboko u nama usled ogromne otuđenosti i povučenosti ljudi. Naša namera je da pokušamo da iskoristimo popularnost koju trenutno ovaj portal uživa, kako bismo dokazali svima da još uvek postoje ti dobri ljudi koji se povlače pred gadostima koje nam se serviraju svakodnevno.',
  'about.vision' : 'Vizija',
  'about.vision.info' : 'Ideja Ispovesti.com je da zabavi i unese radost i ljubav u živote svih nas. Želimo da dokažemo da još uvek postoje iskreni, plemeniti i dobronamerni ljudi kao i svi oni koji zaslužuju šansu da se poprave i promene.',
  'about.regards' : 'Kind Regards, Team TrachHub.com',

  'contact.h1' : 'Contact',
  'contact.write' : 'Write To Us!',
  'contact.write.info1' : 'Ukoliko imate bilo kakvu zamerku, savet, predlog ili pohvalu, slobodno nam pišite na mail',
  'contact.write.info2' : '. Voljni smo da saslušamo mišljenje naših korisnika. ',
  'contact.write.info3' : 'Molimo vas da pre slanja bilo kakvih pitanja, pročitate ',
  'contact.write.info4' : 'i ukoliko ne nađete odgovor, pišete nam putem maila.',
  'contact.write.note' : '*Note:',
  'contact.write.thanks' : 'Thanks!',

  'faq.h1' : 'About Us',
  'faq.question1' : '1. "Zašto ne objavljujete sve ispovesti?"',
  'faq.question1.answer' : 'Objava svih ispovesti nije moguća iz više razloga. Svakodnevno pristiže neverovatno mnogo Vaših ispovesti i potrebno je napraviti selekciju. Neke od njih ne zadovoljavaju osnovna pravila sajta, a sve ostale neobjavljene ispovesti nisu dovoljno dobro ocenjene u sekciji Budi Admin, koja je i uvedena kako bi sami korisnici izabrali ispovesti koje će biti objavljene na sajtu.',
  'faq.question2' : '2. "Zašto moja ispovest nije objavljena a ispunjava sve kriterijume pravilnika?"',
  'faq.question2.answer' : 'Ukoliko ispovest ispunjava sve kriterijume navedene u pravilniku( link ), znači da je ušla u sekciju Budi Admin, ali nije izglasana od strane korisnika, odnosno, postojale su ispovesti koje su bile bolje ocenjene.',
  'faq.question3' : '3. "Zašto ne odobravate sve komentare, iako ne sadrže psovke?"',
  'faq.question3.answer' : ' početka, komentari nisu zahtevali nikakvu moderaciju. Međutim, mnogi su to zloupotrebljavali i bili smo primorani na ovaj korak. Budući da je sve anonimno, svako može da napiše neprimeren sadržaj ili naruši kvalitet sadržaja na sajtu, raznim spam porukama i sl. Nastojimo da odobrimo sve komentare. Međutim, postoje određeni komentari koji za cilj nemaju dobronamernu kritiku, ili savet usmeren ka autoru, već izražavaju govor mržnje i oštro kritikovanje bez konstruktivnih predloga za poboljšanje. Takođe, komentari koji sadrže eksplicitne vulgarnosti (necenzurisane), nisu dobrodošli na sajtu. Ukoliko Vaš komentar nije pušten, a smatrate da je to greška, pošaljite mail na office@ispovesti.com i mi ćemo ga objaviti.',

  'marketing.h1' : 'Marketing',
  'marketing.h3' : 'Oglašavanje na TrachHub.com',
  'marketing.info': 'Za sve informacije u vezi reklamiranja ili sličnih aktivnosti, obratite nam se na',

  'terms.h1' : 'Terms of use',
  'terms.introduction' : 'Introduction',
  'terms.introduction.info': 'TrachHub predstavlja skup raznih ljudskih misljenja, glasina, zargonski receno tracheva, koje mnogi žele da podele sa drugima ali se boje reakcije okoline. TrachHub je namenjen svim grupacijama i ličnostima ljudi i namenjen je za svaki uzrast. Korišćenjem bilo kog dela TrachHub portala i svih njegovih delova, automatski prihvatate sva aktuelna pravila korišćenja. Korisnici su dužni da redovno čitaju pravila korišćenja, te se smatra da su korisnici kontinuiranim korišćenjem TrachHub portala, ili bilo kojeg njegovog dela, u svakom trenutku upoznati s aktuelnim pravilima korišćenja, te da su ih razumeli u celosti. Ni jedan deo TrachHub portala ne sme se koristiti u nezakonite svrhe, ni za promovisanje istih.',
  'terms.portal' : 'TrachHub.com Portal',
  'terms.portal.info': 'TrachHub posetiocima u dobroj nameri i u jedinom cilju zabave obezbeđuje korišćenje sadržaja TrachHub portala. Svi posetioci imaju pravo da besplatno koriste sadržaje TrachHub portala, ukoliko ne krše pravila korišćenja. TrachHub portal sastoji se od vlastitih sadržaja, sadržaja partnera i oglašivača, besplatnih sadržaja, sadržaja kreiranih od strane posetilaca i linkova na spoljne stranice. TrachHub sadržaje objavljuje u dobroj nameri. Sve sadržaje TrachHub portala koristite na vlastitu odgovornost i TrachHub se ne može smatrati odgovornim za bilo kakvu štetu nastalu korišćenjem. Pristup TrachHub portalu i svim njegovim sadržajima dozvoljen je osobama bilo kog uzrasta.',
  'terms.copyright' : 'Copyright',
  'terms.copyright.info': 'TrachHub polaže autorska prava na sve vlastite sadržaje (tekstualne, vizuelne i audio materijale, baze podataka i programerski kod). Neovlašćeno korišćenje bilo kog dela portala smatra se kršenjem autorskih prava TrachHub portala i podložno je tužbi. Neovlašćeno korišćenje bilo kog dela portala bez dozvole vlasnika autorskih prava (TrachHub, partneri, korisnici...) smatra se kršenjem autorskih prava i podložno je tužbi. Ukoliko smatrate da je TrachHub povredio Vaša autorska prava, slučaj će biti odmah razmotren, a sporni sadržaji biće uklonjeni odmah po eventualnom ustanovljavanju istinitosti sadržaja žalbe.',
  'terms.links' : 'External Links',
  'terms.links.info': 'TrachHub sadrži linkove na web stranice izvan vlastitog portala. TrachHub linkove objavljuje u dobroj nameri i ne može se smatrati odgovornim za sadržaje izvan portala',
  'terms.privacy' : 'Privatnost korisnika',
  'terms.privacy.info': 'TrachHub može u skladu sa zakonom prikupljati određene podatke o korisnicima dobijene tokom korišćenja portala (isključivo podaci o računaru, lokaciji i podaci o Internet provajderu). Ove podatke TrachHub koristi kako bi imao informacije o korisnicima koji ga koriste, te na taj način poboljšao portal i njegove sadržaje dodatno usmerio i prilagodio publici koja ga posećuje. TrachHub se obavezuje da će čuvati privatnost korisnika portala, osim u slučaju teškog kršenja pravila TrachHub portala ili nezakonitih aktivnosti korisnika.',
  'terms.rules' : 'Modifikovanje i promene pravila',
  'terms.rules.info': 'Za sve informacije u vezi reklamiranja ili sličnih aktivnosti, obratite nam se na'

};

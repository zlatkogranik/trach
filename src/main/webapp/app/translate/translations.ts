/**
 * Created by ToZla on 19/02/2017.
 */
import {OpaqueToken} from "@angular/core";
import {LANG_RS_TRANS} from "./i18n/lang-rs";
import {LANG_EN_TRANS} from "./i18n/lang-en";

// translation token
export const TRANSLATIONS = new OpaqueToken('translations');

// all traslations
export const dictionary = {
  'en': LANG_EN_TRANS,
  'rs': LANG_RS_TRANS
};

// providers
export const TRANSLATION_PROVIDERS = [
  {provide: TRANSLATIONS, useValue: dictionary},
];

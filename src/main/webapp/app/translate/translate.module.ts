/**
 * Created by ToZla on 19/02/2017.
 */
import {NgModule} from "@angular/core";
import {TranslateService} from "./translate.service";
import {TranslatePipe} from "./translate.pipe";
import {TRANSLATION_PROVIDERS} from "./translations";

@NgModule({
  imports: [],
  exports: [TranslatePipe],
  declarations: [TranslatePipe],
  providers: [TranslateService, TRANSLATION_PROVIDERS],
})
export class TranslateModule {

}

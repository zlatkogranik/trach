/**
 * Created by ToZla on 11/02/2017.
 */
import {Component} from "@angular/core";
import {Router, NavigationEnd} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'trach-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.css']
})
export class HeaderComponent {

  public phrase: string;

  private currentUrl: string;

  constructor(private router: Router) {

    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = event.url;
        this.currentUrl = this.currentUrl.replace('/date', '').replace('/likes', '').replace('/unlikes', '');
      }
    });

  }

  search() {

    this.router.navigate(["search/" + this.phrase]);

    this.phrase = null;

  }

  showSortMenu() {
    return this.currentUrl != null && (this.currentUrl.startsWith('/home') || this.currentUrl.startsWith('/type/'));
  }

  sort(type: string) {

    switch (type) {
      case 'newest':
        this.router.navigate([this.currentUrl + '/date']);
        break;
      case 'likes':
        this.router.navigate([this.currentUrl + '/likes']);
        break;
      case 'dislikes':
        this.router.navigate([this.currentUrl + '/unlikes']);
        break;
    }

  }

}

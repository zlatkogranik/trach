﻿import {Injectable} from "@angular/core";
import {Router, CanActivate} from "@angular/router";
import {LocalStorageService} from "../_base/local-storage/LocalStorageService";
import {XC} from "../_base/local-storage/LocalStorageAttributes";


@Injectable()
export class AuthGuard implements CanActivate {

  private xc: string;

  constructor(private router: Router) {
    this.xc = LocalStorageService.get(XC);
  }

  canActivate() {
    if (this.xc != undefined) {
      return true;
    }
    this.router.navigate(['/']);
    return false;
  }

}

/**
 * Created by ToZla on 17/02/2017.
 */
import {Component, OnInit} from "@angular/core";
import {StatisticsService} from "../statistics/statistics.service";
import {Statistics} from "../statistics/statistics";

@Component({
  moduleId: module.id,
  selector: 'trach-statistics',
  templateUrl: 'trach-statistics.component.html',
  styleUrls: ['trach-statistics.component.css']
})
export class TrachStatisticsComponent implements OnInit {

  public statistics: Statistics;

  constructor(private statisticsService: StatisticsService) {

  }

  ngOnInit() {
    this.statisticsService.getStatistics()
      .subscribe(response => {
        this.statistics = response;
      })
  }

}

/**
 * Created by ToZla on 17/02/2017.
 */
import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {Trach} from "./trach";

@Component({
  moduleId: module.id,
  selector: 'trach-list',
  templateUrl: 'trach-list.component.html'
})
export class TrachListComponent implements OnInit {

  @Input() traches: Trach[];

  @Output() loadNext: EventEmitter<any> = new EventEmitter();

  constructor() {

  }

  ngOnInit() {

  }

}

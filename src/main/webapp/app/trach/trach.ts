import {Comment} from "../comment/comment";
import {Tag} from "../tag/tag";
/**
 * Created by ToZLa on 1/31/2017.
 */
export class Trach {

  public id?: number;
  public text: string;
  public date?: Date;
  public tags?: Tag[];
  public likes?: number = 0;
  public unlikes?: number = 0;
  public shares?: number = 0;
  public comment?: number = 0;
  public comments?: Comment[] = [];

}

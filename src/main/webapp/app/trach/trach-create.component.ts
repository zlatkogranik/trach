/**
 * Created by ToZla on 11/02/2017.
 */
import {Component} from "@angular/core";
import {TrachService} from "./trach.service";

@Component({
  moduleId: module.id,
  selector: 'trach-trach-create',
  templateUrl: 'trach-create.component.html'
})
export class TrachCreateComponent {

  public text: string;

  constructor(private trachService: TrachService) {

  }

  save() {

    if (this.text == null || this.text.length < 5) {
      return;
    }

    this.trachService.save({text: this.text})
      .subscribe(trach => {
        this.text = null;
      });

  }

  onKeyDown(event: any) {


  }

}

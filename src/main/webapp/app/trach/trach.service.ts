/**
 * Created by ToZLa on 1/31/2017.
 */
import {Injectable} from "@angular/core";
import {Trach} from "./trach";
import {Observable} from "rxjs";
import {HttpService} from "../_shared/common/http/HttpService";
import {Page} from "../_shared/common/page";

@Injectable()
export class TrachService {

  private url = 'api/trach';  // URL to web api

  constructor(private http: HttpService) {

  }

  getTrachById(id: number) {
    return this.http.get(this.url + "/" + id)
      .map(response => <Trach>response.json());
  }

  getTrachesPageable(page: any): Observable<Page<Trach>> {
    return this.http.post(this.url + "/pageable", page)
      .map(response => <Page<Trach>>response.json());
  }

  save(trach: Trach): Observable<Trach> {
    return this.http.post(this.url, trach)
      .map(response => <Trach>response.json());
  }

  like(trach: Trach): Observable<Trach> {
    return this.http.post(this.url + "/like", trach)
      .map(response => <Trach>response.json());
  }

  unlike(trach: Trach): Observable<Trach> {
    return this.http.post(this.url + "/unlike", trach)
      .map(response => <Trach>response.json());
  }

  share(trach: Trach): Observable<Trach> {
    return this.http.post(this.url + "/share", trach)
      .map(response => <Trach>response.json());
  }

}

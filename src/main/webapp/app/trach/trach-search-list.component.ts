/**
 * Created by ToZla on 17/02/2017.
 */
import {Component, OnInit} from "@angular/core";
import {TrachService} from "./trach.service";
import {ActivatedRoute} from "@angular/router";
import {Trach} from "./trach";
import {TagService} from "../tag/tag.service";
import {Tag} from "../tag/tag";
import {PageFilter} from "../_shared/common/page-filter";
import {PageFilterMetadata} from "../_shared/common/page-filter-metadata";

@Component({
  moduleId: module.id,
  selector: 'trach-search-list',
  templateUrl: 'trach-search-list.component.html'
})
export class TrachSearchListComponent implements OnInit {

  private id: number;
  private tag: Tag;
  public traches: Trach[];

  private page: PageFilter;

  constructor(private trachService: TrachService,
              private tagService: TagService,
              private route: ActivatedRoute) {

  }

  ngOnInit() {

    this.route.params.subscribe(params => {

        this.page = new PageFilter(0, 10, [], null, null, []);

        this.traches = [];

        this.id = +params['id'];
        if (this.id) {
          this.trachService.getTrachById(this.id)
            .subscribe(trach => {
              this.traches = [trach];
            });
        }

        let tag = params['tag'];

        if (tag) {

          this.page.sortOrder = -1;

          this.tagService.getTagByText(tag)
            .subscribe(response => {
              this.tag = response;
              this.page.tags = [this.tag.id];
              this.trachService.getTrachesPageable(this.page)
                .subscribe(page => {
                  this.traches = page.content;
                });
            })


        }

        let type = params['type'];

        if (type) {

          let sort = params['sort'];

          if (sort) {
            this.page.sortField = sort;
          } else {
            this.page.sortField = 'likes';
          }

          this.page.sortOrder = -1;

          switch (type) {
            case 'popular':
              break;
            case 'today':
              this.page.filterMetadata = [new PageFilterMetadata('category', 'DAY')];
              break;
            case 'month':
              this.page.filterMetadata = [new PageFilterMetadata('category', 'MONTH')];
              break;
          }

          this.trachService.getTrachesPageable(this.page)
            .subscribe(page => {
              this.traches = page.content;
            });

        }

        let search = params['search'];

        if (search) {

          if (search.startsWith("#")) {
            this.tagService.getTagByText(search)
              .subscribe(response => {
                this.tag = response;
                this.page.tags = [this.tag.id];
                this.trachService.getTrachesPageable(this.page)
                  .subscribe(page => {
                    this.traches = page.content;
                  });
              })
          } else {
            this.page.filterMetadata = [new PageFilterMetadata('search', search)];

            this.trachService.getTrachesPageable(this.page)
              .subscribe(page => {
                this.traches = page.content;
              });
          }

        }


      }
    );

  }

  nextPage() {

    this.page.firstResult += 10;

    this.trachService.getTrachesPageable(this.page)
      .subscribe(page => {
        this.traches.push.apply(this.traches, page.content);
      })

  }

}

/**
 * Created by ToZLa on 1/31/2017.
 */
import {Component, OnInit, Input} from "@angular/core";
import {Trach} from "./trach";
import {Tag} from "../tag/tag";
import {ActivatedRoute, Router} from "@angular/router";
import {TrachService} from "./trach.service";
import {Comment} from "../comment/comment";
import {CommentService} from "../comment/comment.service";
import {FacebookService, FacebookInitParams} from "../_shared/common/fb/facebook-sdk";

@Component({
  moduleId: module.id,
  selector: 'trach-details',
  templateUrl: 'trach-details.component.html',
  styleUrls: ['trach-details.component.css']
})
export class TrachDetailsComponent implements OnInit {

  @Input() trach: Trach;

  private id: number;

  private comment: Comment = new Comment();

  private commentsLoaded: boolean = false;
  private commentsExpanded: boolean = false;

  constructor(private trachService: TrachService,
              private route: ActivatedRoute,
              private router: Router,
              private commentService: CommentService,
              private facebookService: FacebookService) {

    let fbParams: FacebookInitParams = {
      appId: '1687042191588567',
      xfbml: true,
      version: 'v2.8'
    };
    this.facebookService.init(fbParams);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      if (this.id) {
        this.trachService.getTrachById(this.id)
          .subscribe(trach => {
            this.trach = trach;
          });
      }
    });
  }

  loadComments() {

    if (this.commentsLoaded) {
      this.commentsExpanded = !this.commentsExpanded;
    } else {

      this.commentService.findCommentsByTrachId(this.trach.id)
        .subscribe(comments => {
          this.trach.comments = comments;
          this.commentsLoaded = true;
        })

      this.commentsExpanded = true;

    }

  }

  searchByTag(tag: Tag) {
    this.router.navigate(["tag/" + tag.text]);
  }

  like() {
    this.trachService.like(this.trach)
      .subscribe(response => {
        this.trach.likes = response.likes;
        this.trach.unlikes = response.unlikes;
      })
  }

  unlike() {
    this.trachService.unlike(this.trach)
      .subscribe(response => {
        this.trach.likes = response.likes;
        this.trach.unlikes = response.unlikes;
      })
  }

  share() {

    this.trachService.share(this.trach)
      .subscribe(response => {
        this.trach.shares++;
      });

    this.facebookService.ui({
      display: 'popup',
      method: 'share',
      href: 'http://trachhub.com/trach/' + this.trach.id,
      quote: this.trach.text
    }).then((response: any) => {
        this.trachService.share(this.trach);
      },
      (error: any) => console.error(error)
    );
  }

  saveComment() {

    if (this.comment.text == null || this.comment.text.length < 4) {
      return;
    }

    this.commentService.saveComment(this.trach, this.comment)
      .subscribe(comment => {
        this.comment = new Comment();
      })
  }

  likeComment(comment: Comment) {
    this.commentService.like(comment)
      .subscribe(response => {
        comment.likes = response.likes;
        comment.unlikes = response.unlikes;
      })
  }

  unlikeComment(comment: Comment) {
    this.commentService.unlike(comment)
      .subscribe(response => {
        comment.unlikes = response.unlikes;
        comment.likes = response.likes;
      })
  }
}

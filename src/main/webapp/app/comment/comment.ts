/**
 * Created by ToZLa on 1/31/2017.
 */
export class Comment {

  id: number;
  date: Date;
  text: string;
  author: string;
  likes: number;
  unlikes: number;

}

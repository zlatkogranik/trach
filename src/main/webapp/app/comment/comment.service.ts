/**
 * Created by ToZla on 17/02/2017.
 */
import {Injectable} from "@angular/core";
import {HttpService} from "../_shared/common/http/HttpService";
import {Observable} from "rxjs";
import {Trach} from "../trach/trach";
import {Comment} from "./comment";

@Injectable()
export class CommentService {

  private url = 'api/comment';  // URL to web api

  constructor(private http: HttpService) {

  }

  findCommentsByTrachId(id: number): Observable<Comment[]> {
    return this.http.get(this.url + "/trach/" + id)
      .map(response => <Comment[]>response.json());
  }

  saveComment(trach: Trach, comment: Comment): Observable<Comment> {
    return this.http.post(this.url + "/" + trach.id, comment)
      .map(response => <Comment>response.json());
  }

  like(comment: Comment): Observable<Comment> {
    return this.http.post(this.url + "/like", comment)
      .map(response => <Comment>response.json());
  }

  unlike(comment: Comment): Observable<Comment> {
    return this.http.post(this.url + "/unlike", comment)
      .map(response => <Comment>response.json());
  }

  getCommentById(id: number): Observable<Comment> {
    return this.http.get(this.url + "/" + id)
      .map(response => <Comment>response.json());
  }

}

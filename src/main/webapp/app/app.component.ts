import {Component, OnInit} from "@angular/core";
import {TranslateService} from "./translate/translate.service";

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {

  public title = 'Trach!';
  public supportedLanguages: any[];

  constructor(private translateService: TranslateService) {

  }

  ngOnInit() {
    // standing data
    this.supportedLanguages = [
      {display: 'English', value: 'en'},
      {display: 'Serbian', value: 'rs'}
    ];

    this.selectLang('en');

  }

  isCurrentLang(lang: string) {
    return lang === this.translateService.currentLang;
  }

  selectLang(lang: string) {
    this.translateService.use(lang);
    this.refreshText();
  }

  refreshText() {
    this.title = this.translateService.instant('title');
  }

}

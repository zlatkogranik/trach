/**
 * Created by ToZLa on 1/31/2017.
 */
import {Component, OnInit} from "@angular/core";
import {TrachService} from "../trach/trach.service";
import {Trach} from "../trach/trach";
import {TagService} from "../tag/tag.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'trach-home-page',
  templateUrl: 'home-page.component.html'
})
export class HomePageComponent implements OnInit {

  public traches: Trach[];

  private page: any = {firstResult: 0, maxResult: 10};

  constructor(private trachService: TrachService,
              private route: ActivatedRoute) {

  }

  ngOnInit() {

    this.route.params.subscribe(params => {

      let sort = params['sort'];

      if (sort) {
        this.page.sortField = sort;
      } else {
        this.page.sortField = 'date';
      }

      this.page.sortOrder = -1;

      this.trachService.getTrachesPageable(this.page)
        .subscribe(page => {
          this.traches = page.content;
        })

    });

  }

  nextPage() {

    this.page.firstResult += 10;

    this.trachService.getTrachesPageable(this.page)
      .subscribe(page => {
        this.traches.push.apply(this.traches, page.content);
      })

  }

}

/**
 * Created by ToZla on 10/03/2017.
 */
import {Component, OnInit} from "@angular/core";
import {CommentService} from "../../comment/comment.service";
import {AdminService} from "../admin.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Comment} from "../../comment/comment";

@Component({
  moduleId: module.id,
  selector: 'admin-comment-details',
  templateUrl: 'admin-comment-details.component.html'
})
export class AdminCommentDetailsComponent implements OnInit {

  id: number;
  comment: Comment;

  constructor(private commentService: CommentService,
              private adminService: AdminService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      if (this.id) {
        this.commentService.getCommentById(this.id)
          .subscribe(comment => {
            this.comment = comment;
          });
      }
    });
  }

  save() {
    this.adminService.updateComment(this.comment)
      .subscribe(comment => {
        if (comment.id > 0) {
          this.cancel();
        }
      });
  }

  cancel() {
    this.router.navigate(["admin/home"]);
  }
}

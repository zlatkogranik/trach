/**
 * Created by ToZla on 19/02/2017.
 */
import {Injectable} from "@angular/core";
import {HttpService} from "../_shared/common/http/HttpService";
import {Observable} from "rxjs";
import {Page} from "../_shared/common/page";
import {Trach} from "../trach/trach";
import {Comment} from "../comment/comment";

@Injectable()
export class AdminService {

  private trachUrl = 'api/admin/trach';  // URL to web api
  private commentUrl = 'api/admin/comment';  // URL to web api

  constructor(private http: HttpService) {

  }

  getTrachesPageable(page: any): Observable<Page<Trach>> {
    return this.http.post(this.trachUrl + "/pageable", page)
      .map(response => <Page<Trach>>response.json());
  }

  getCommentsPageable(page: any): Observable<Page<Comment>> {
    return this.http.post(this.commentUrl + "/pageable", page)
      .map(response => <Page<Comment>>response.json());
  }

  approveTrach(trach: Trach): Observable<Trach> {
    return this.http.put(this.trachUrl + "/approve", trach)
      .map(response => <Trach>response.json());
  }

  approveComment(comment: Comment): Observable<Comment> {
    return this.http.put(this.commentUrl + "/approve", comment)
      .map(response => <Comment>response.json());
  }

  updateComment(comment: Comment): Observable<Comment> {
    return this.http.put(this.commentUrl, comment)
      .map(response => <Comment>response.json());
  }

  updateTrach(trach: Trach): Observable<Trach> {
    return this.http.put(this.trachUrl, trach)
      .map(response => <Trach>response.json());
  }

  rejectTrach(trach: Trach): Observable<Trach> {
    return this.http.put(this.trachUrl + "/reject", trach)
      .map(response => <Trach>response.json());
  }

  rejectComment(comment: Comment): Observable<Comment> {
    return this.http.put(this.commentUrl + "/reject", comment)
      .map(response => <Comment>response.json());
  }
}

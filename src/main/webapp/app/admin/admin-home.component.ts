/**
 * Created by ToZla on 19/02/2017.
 */
import {Component, OnInit} from "@angular/core";
import {Trach} from "../trach/trach";
import {AdminService} from "./admin.service";
import {Comment} from "../comment/comment";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'admin-home',
  templateUrl: 'admin-home.component.html',
  styleUrls: ['admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {

  public activeTab = 1;

  public traches: Trach[];
  public comments: Comment[];

  private trachPage: any = {firstResult: 0, maxResult: 10};
  private commentPage: any = {firstResult: 0, maxResult: 10};

  constructor(private adminService: AdminService,
              private router: Router) {

  }

  ngOnInit() {
    this.adminService.getTrachesPageable(this.trachPage)
      .subscribe(page => {
        this.traches = page.content;
      })
    this.adminService.getCommentsPageable(this.commentPage)
      .subscribe(page => {
        this.comments = page.content;
      })
  }

  editTrach(trach: Trach) {
    this.router.navigate(["admin/trach/" + trach.id]);
  }

  editComment(comment: Comment) {
    this.router.navigate(["admin/comment/" + comment.id]);
  }

  approveTrach(trach: Trach) {
    this.adminService.approveTrach(trach)
      .subscribe(response => {
        if (response) {
          for (let i = 0; i < this.traches.length; i++) {
            if (this.traches[i].id == response.id) {
              this.traches.splice(i, 1);
              break;
            }
          }
        }
      });
  }

  rejectTrach(trach: Trach) {
    this.adminService.rejectTrach(trach)
      .subscribe(response => {
        if (response) {
          for (let i = 0; i < this.traches.length; i++) {
            if (this.traches[i].id == response.id) {
              this.traches.splice(i, 1);
              break;
            }
          }
        }
      });
  }

  approveComment(comment: Comment) {
    this.adminService.approveComment(comment)
      .subscribe(response => {
        if (response) {
          for (let i = 0; i < this.comments.length; i++) {
            if (this.comments[i].id == response.id) {
              this.comments.splice(i, 1);
              break;
            }
          }
        }
      });
  }

  rejectComment(comment: Comment) {
    this.adminService.rejectComment(comment)
      .subscribe(response => {
        if (response) {
          for (let i = 0; i < this.comments.length; i++) {
            if (this.comments[i].id == response.id) {
              this.comments.splice(i, 1);
              break;
            }
          }
        }
      });
  }

  activateTrachTab() {
    this.activeTab = 1;
  }

  activateCommentTab() {
    this.activeTab = 2;
  }

  nextPageTrach() {

  }

  nextPageComment() {

  }

}

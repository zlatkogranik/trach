/**
 * Created by ToZla on 10/03/2017.
 */
import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Trach} from "../../trach/trach";
import {AdminService} from "../admin.service";
import {TrachService} from "../../trach/trach.service";

@Component({
  moduleId: module.id,
  selector: 'admin-trach-details',
  templateUrl: 'admin-trach-details.component.html'
})
export class AdminTrachDetailsComponent implements OnInit {

  id: number;
  trach: Trach;

  constructor(private trachService: TrachService,
              private adminService: AdminService,
              private route: ActivatedRoute,
              private router: Router) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      if (this.id) {
        this.trachService.getTrachById(this.id)
          .subscribe(trach => {
            this.trach = trach;
          });
      }
    });
  }

  save() {
    this.adminService.updateTrach(this.trach)
      .subscribe(trach => {
        if (trach.id > 0) {
          this.cancel();
        }
      });
  }

  cancel() {
    this.router.navigate(["admin/home"]);
  }

}

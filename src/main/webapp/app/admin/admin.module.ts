/**
 * Created by ToZla on 19/02/2017.
 */
import {NgModule} from "@angular/core";
import {AdminHomeComponent} from "./admin-home.component";
import {AdminService} from "./admin.service";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [CommonModule],
  exports: [AdminHomeComponent],
  declarations: [AdminHomeComponent],
  providers: [AdminService],
})
export class AdminModule {
}

/**
 * Created by ToZLa on 1/31/2017.
 */
import {Component, OnInit} from "@angular/core";
import {LocalStorageService} from "../_base/local-storage/LocalStorageService";
import {ActivatedRoute} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'trach-local-storage',
  template: ``
})
export class LocalStorageComponent implements OnInit {

  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {

    this.route.params.subscribe(params => {

      let key = params['key'];
      let value = params['value'];

      if (key && value) {
        LocalStorageService.put(key, value);
      }

    });

  }

}

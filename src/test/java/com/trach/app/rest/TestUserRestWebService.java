package com.trach.app.rest;

import com.trach.app.base.BaseRestTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sun.security.acl.PrincipalImpl;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestUserRestWebService extends BaseRestTest {

  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext wac;

  @Before
  public void init() {
    mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @Test
  public void testCreateUser() throws Exception {
    mockMvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON)
                                 .content("{\"plainTextPassword\": \"Password5\", \"email\": \"test@gmail.com\"}")
                                 .accept(MediaType.APPLICATION_JSON)
                                 .principal(new PrincipalImpl("")))
           .andDo(print())
           .andExpect(status().isOk());

  }

}

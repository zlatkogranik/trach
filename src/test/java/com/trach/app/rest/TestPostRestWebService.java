package com.trach.app.rest;

import com.trach.app.base.BaseRestTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sun.security.acl.PrincipalImpl;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TestPostRestWebService extends BaseRestTest {

  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext wac;

  @Before
  public void init() {
    mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @Test
  public void testSavePost() throws Exception {
    mockMvc.perform(post("/post").contentType(MediaType.APPLICATION_JSON)
                                 .content("{\"date\": \"2015/01/01\",\"time\": \"11:00\", \"description\": \"test\" }")
                                 .accept(MediaType.APPLICATION_JSON)
                                 .principal(new PrincipalImpl("")))
           .andDo(print())
           .andExpect(status().isOk())
           .andExpect(content().contentType("application/json;charset=UTF-8"))
           .andExpect(jsonPath("$.['description']").value("test"));
  }

  @Test
  public void deletePost() throws Exception {
    mockMvc.perform(delete("/post").contentType(MediaType.APPLICATION_JSON)
                                   .content("14")
                                   .accept(MediaType.APPLICATION_JSON)
                                   .principal(new PrincipalImpl("")))
           .andDo(print())
           .andExpect(status().isOk());

  }

}

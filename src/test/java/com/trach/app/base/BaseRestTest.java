package com.trach.app.base;

import com.trach.config.root.ApplicationProperties;
import com.trach.config.root.RootContextConfig;
import com.trach.config.root.TestConfiguration;
import com.trach.config.servlet.ServletContextConfig;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ActiveProfiles("test")
@ContextConfiguration(classes = { TestConfiguration.class, RootContextConfig.class, ApplicationProperties.class, ServletContextConfig.class })
public class BaseRestTest {

}

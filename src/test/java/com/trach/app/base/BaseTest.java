package com.trach.app.base;

import com.trach.config.root.ApplicationProperties;
import com.trach.config.root.RootContextConfig;
import com.trach.config.root.TestConfiguration;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = { TestConfiguration.class, RootContextConfig.class, ApplicationProperties.class })
public class BaseTest {

}
